<?php

App::uses('Sanitize', 'Utility');
class Listing extends AppModel
{

    public $useTable = false;

    public $default_filters = "
        StateOrProvince Eq 'FL' And MlsStatus Eq 'Active' And 
        PropertyClass Eq 'Residential' And MajorChangeType Eq 'New Listing'";

    public function getOurRecent($flex, $count)
    {
        $listings = $flex->getMyListings([
            '_limit' => $count,
            '_expand' => 'Photos',
            '_filter' => $this->default_filters,
            '_order' => 'Id Descending'
        ]);

        if (empty($listings) || $listings == false) {
            return false;
        }

        foreach ($listings as $listing) {
            self::saveViewed($listing);
        }

        return $listings;
    }

    public function getRecent($flex, $count)
    {
        $listings = $flex->GetMyListings([
            '_limit' => $count,
            '_expand' => 'Photos',
            '_filter' => $this->default_filters,
            '_order' => 'Id Descending'
        ]);

        if (empty($listings) || $listings == false) {
            return false;
        }

        foreach ($listings as $listing) {
            self::saveViewed($listing);
        }

        return $listings;
    }

    public function saveViewed($data) {
        $info = array(
            'Listing' => array(
                'ListingId' => $data['StandardFields']['ListingId'],
                'StreetNumber' => $data['StandardFields']['StreetNumber'],
                'StreetName' => $data['StandardFields']['StreetName'],
                'StreetSuffix' => $data['StandardFields']['StreetSuffix'],
                'City' => $data['StandardFields']['City'],
                'StateOrProvince' => $data['StandardFields']['StateOrProvince'],
                'PostalCode' => $data['StandardFields']['PostalCode'],
            )
        );

        // check if listing exists
        $exists = $this->find('first', array(
            'conditions' => array('Listing.ListingId' => $data['StandardFields']['ListingId'])
        ));

        // save the listing details
        if (!$exists) {
            $this->create();
            $this->save($info);
        }

    }


}