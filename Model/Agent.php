<?php

class Agent extends AppModel {

  public function featured () {
    return $this->find('all', array(
      'conditions' => array(
        'Agent.featured' => 1
      ),
      'order' => 'rand()',
      'limit' => 3
    ));
  }

}