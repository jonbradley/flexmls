<?php

class FeaturedListing extends AppModel {

  public function featured_listings ($flex) {

    $data = $this->find('all', array(
      'order' => 'rand()',
      'limit' => 5
    ));

    $listings = array();

    foreach ($data as $key => $value) {

      $conditions = array(
        '_limit'  => 1,
        '_expand' => "Photos",
        '_filter' => "ListingId Eq '". $value['FeaturedListing']['mls'] ."'",
      );

      $search = $flex->GetListings($conditions);

      if (!empty($search)) {
        $result = $flex->GetListing($search[0]['Id'], array(
          '_expand' => 'Photos',
        ));
        
        if (!empty($result))
          $listings[] = $result[0];
      }

    }

    $count = count($listings);
    if ($count > 3) {
      $remove = $count - 3;
      for ($i = 3; $i <= $count; $i++) {
        unset($listings[$i]);
      }
    }

    foreach ($listings as $listing) {
      ClassRegistry::init('Listing')->saveViewed($listing);
    }

    return $listings;
  }

}