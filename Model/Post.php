<?php

class Post extends AppModel {

  public $useTable = 'posts';

  public function recent_posts () {
    return $this->find('all', array(
      'order' => array('Post.id DESC'),
      'limit' => 4
    ));
  }

}