<?php

class Page extends AppModel {

  public $useTable = 'pages';

  public function get_seo ($slug) {
    $data =  $this->find('first', array(
      'conditions' => array('Page.slug' => $slug),
      'order' => array('Page.id DESC'),
      'limit' => 4
    ));
    if (!empty($data)) {
      return array(
        'title' => $data['Page']['seo_title'],
        'description' => $data['Page']['seo_description'],
        'keywords' => $data['Page']['seo_keywords'],
      );
    }
  }

}