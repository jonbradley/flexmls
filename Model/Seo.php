<?php

App::uses('Sanitize', 'Utility');
class Seo extends AppModel {

  public $useTable = 'seo';

  public function getSeo ($page) {
    $data = $this->find('first', array(
      'conditions' => array('name' => Sanitize::clean($page)),
      'fields' => array('keywords', 'description', 'title')
    ));

    $info = array();
    if (!empty($data)) {
      $info['keywords'] = $data['Seo']['keywords'];
      $info['title'] = $data['Seo']['title'];
      $info['description'] = $data['Seo']['description'];
    }

    return (!empty($info)) ? $info : array('keywords', 'description', 'title');
  }

}