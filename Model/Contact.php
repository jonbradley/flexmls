<?php

class Contact extends AppModel {

  public $validate = array(
    'name' => array(
      'rule' => 'notEmpty',
      'message' => 'Sorry you must fill out your name.'
    ),
    'email' => array(
      'rule' => 'email',
      'message' => 'Sorry you must fill out a valid email.'
    ),
    'phone_number' => array(
      'rule' => array('phone', null, 'us'),
      'message' => 'Sorry you must fill out a valid phone number.'
    ),
    'best_time_to_contact' => array(
      'rule' => 'notEmpty',
      'message' => 'Sorry you must fill out a time to contact you.'
    ),
  );

}