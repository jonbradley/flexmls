<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
  Router::parseExtensions('html', 'rss');
  Router::connect('/', array('controller' => 'listings', 'action' => 'index'));
  Router::connect('/cache', array('controller' => 'cache', 'action' => 'index'));
  Router::connect('/admin', array('controller' => 'users', 'action' => 'dashboard', 'prefix' => 'admin', 'admin' => true)); 
  Router::connect('/admin/:controller/:action/*', array('prefix' => 'admin', 'admin' => true)); 
  Router::connect('/listings/*', array('controller' => 'listings'));
  Router::connect('/most_recent', array('controller' => 'listings', 'action' => 'most_recent'));
  Router::connect('/most_recent/*', array('controller' => 'listings', 'action' => 'most_recent'));
  Router::connect('/our_most_recent', array('controller' => 'listings', 'action' => 'our_most_recent'));
  Router::connect('/our_most_recent/*', array('controller' => 'listings', 'action' => 'our_most_recent'));
  Router::connect('/search', array('controller' => 'listings', 'action' => 'search'));
  Router::connect('/search/*', array('controller' => 'listings', 'action' => 'search'));
  Router::connect('/search_ours', array('controller' => 'listings', 'action' => 'search_ours'));
  Router::connect('/search_ours/*', array('controller' => 'listings', 'action' => 'search_ours'));
  Router::connect('/agents', array('controller' => 'agents', 'action' => 'index'));
  Router::connect('/agents/:slug', array('controller' => 'agents', 'action' => 'view'));
  Router::connect('/blog', array('controller' => 'posts', 'action' => 'index'));
  Router::connect('/blog/:slug', array('controller' => 'posts', 'action' => 'view'));
  Router::connect('/listings', array('controller' => 'listings', 'action' => 'index'));
  Router::connect('/listings/search/*', array('controller' => 'listings', 'action' => 'search'));
  Router::connect('/:city/:id/:address', array('controller' => 'listings', 'action' => 'view'));
  Router::connect('/:slug', array('controller' => 'pages', 'action' => 'view'));
  Router::connect('/contact/:slug', array('controller' => 'contacts', 'action' => 'view'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
