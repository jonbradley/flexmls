<?php


App::uses('Controller', 'Controller');
App::uses('Flex', 'Vendor');

class AppController extends Controller
{

    public static $tokens = array(
        'FlexMLS' => array(
            'key' => 'fl_o303601_key_1',
            'secret' => 'OsuBBX2Ub0py0AXc6Neqg'
        ),
        'Zillow' => array(
            'id' => 'X1-ZWz1ddh5gi2nez_211k7'
        )
    );

    public $components = array(
        // 'DebugKit.Toolbar',
        'Session',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'users', 'action' => 'dashboard', 'prefix' => 'admin', 'admin' => true),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login', 'prefix' => 'admin', 'admin' => true)
        )
    );

    public $page_name = 'default';

    public function beforeFilter()
    {
        $this->Flex = new Flex(self::$tokens['FlexMLS']['key'], self::$tokens['FlexMLS']['secret']);
        $this->Flex->setApplicationName("RealEstateDiamond2/1.0");
        self::__setFooter();
        self::__setFilters();
    }

    public function beforeRender()
    {
        $this->seo = ClassRegistry::init('Seo')->getSeo($this->page_name);

        if ($this->request->params['controller'] == 'pages' && $this->request->params['action'] == 'view') {
            $this->seo = ClassRegistry::init('Page')->get_seo($this->request->params['slug']);
        }

        $this->set('page_name', $this->seo);
    }

    public function isAuthorized()
    {
        $this->layout = 'admin';
        $user = $this->Session->read('Auth.User');
        // Admin can access every action
        if (isset($user['role']) && $user['role'] === 'admin') {
            return true;
        }

        // Default deny
        return false;
    }

    protected function __setFilters()
    {
        $filter['cities'] = ClassRegistry::init('City')->getFilterList();
        $filter['zipcodes'] = ClassRegistry::init('PostalCode')->getFilterList();
        $filter['counties'] = ClassRegistry::init('County')->getFilterList();
        $this->set(compact('filter'));
    }

    protected function __setFooter()
    {
        // load the Modals Required
        Controller::loadModel('Agent');
        Controller::loadModel('Listing');
        Controller::loadModel('Post');
        Controller::loadModel('FeaturedListing');

        $recent_listings      = $this->Listing->getRecent($this->Flex, 3);
        $our_recent_listings  = $this->Listing->getOurRecent($this->Flex, 3);
        $featured_agents      = $this->Agent->featured();
        $recent_posts         = $this->Post->recent_posts();
        $featured_listings    = $this->FeaturedListing->featured_listings($this->Flex);

        $this->set(compact('recent_listings', 'our_recent_listings', 'featured_agents', 'featured_listings', 'recent_posts'));
    }
}
