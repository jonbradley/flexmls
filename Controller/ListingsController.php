<?php

App::uses('AppController', 'Controller');
App::uses('Xml', 'Utility');
App::uses('Sanitize', 'Utility');
App::uses('Zillow', 'Vendor');

class ListingsController extends AppController
{

    public $helpers = array('Link', 'GoogleMap');
    public $uses = [];
    public $page_name = 'listings';

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('index', 'search', 'view', 'dev', 'search_ours', 'most_recent', 'our_most_recent');
    }

    public function beforeRender()
    {
        parent::beforeRender();
    }

    public function index()
    {
        $this->page_name = 'default';
        $listings = $this->Listing->getOurRecent($this->Flex, 12);

        if (empty($listings) || $listings == false) {
            $listings = [];
        }

        foreach ($listings as $listing) {
            $this->Listing->saveViewed($listing);
        }

        $this->set(compact('listings'));
    }

    public function search($page = 1)
    {
        $conditions = self::__setSearch($page);

        $listings = $this->Flex->GetListings($conditions);

        foreach ($listings as $listing) {
            $this->Listing->saveViewed($listing);
        }

        $data = array(
            'data' => $listings,
            'paginate' => $this->Flex->paginate
        );

        $this->set(compact('data'));
    }

    public function search_ours($page = 1)
    {
        $conditions = self::__setSearch($page);

        $listings = $this->Flex->GetMyListings($conditions);

        $data = ['data', 'paginate'];

        if (!empty($listings)) {
            foreach ($listings as $listing) {
                $this->Listing->saveViewed($listing);
            }

            $data = [
                'data' => $listings,
                'paginate' => $this->Flex->paginate
            ];
        }

        $this->set(compact('data'));
    }

    public function most_recent($page = 1)
    {

        $conditions = array(
            '_page' => $page,
            '_limit' => 12,
            '_pagination' => 1,
            '_expand' => 'Photos',
            '_filter' => "StateOrProvince Eq 'FL' And MlsStatus Eq 'Active' And 
                PropertyClass Eq 'Residential' And MajorChangeType Eq 'New Listing'",
            '_order' => 'Id Descending'
        );

        $listings = $this->Flex->GetListings($conditions);

        foreach ($listings as $listing) {
            $this->Listing->saveViewed($listing);
        }

        $data = array(
            'data' => $listings,
            'paginate' => $this->Flex->paginate
        );

        $params = '';

        $this->set(compact('data', 'params'));
    }

    public function our_most_recent($page = 1)
    {

        $conditions = array(
            '_page' => $page,
            '_limit' => 12,
            '_pagination' => 1,
            '_expand' => 'Photos',
            '_filter' => "StateOrProvince Eq 'FL' And MlsStatus Eq 'Active' And 
                PropertyClass Eq 'Residential' And MajorChangeType Eq 'New Listing'",
            '_order' => 'Id Descending'
        );

        $listings = $this->Flex->GetMyListings($conditions);

        foreach ($listings as $listing) {
            $this->Listing->saveViewed($listing);
        }

        $data = array(
            'data' => $listings,
            'paginate' => $this->Flex->paginate
        );

        $params = '';

        $this->set(compact('data', 'params'));
    }

    public function view()
    {
        $this->cacheAction = true;
        $url = 'http://'. $_SERVER['HTTP_HOST'] .'/'. $this->request->url;
        $request = Sanitize::clean($this->request->params);

        // get the information about the listing
        $conditions = array(
            '_limit'  => 1,
            '_expand' => "Photos",
            '_filter' => "ListingId Eq '". $request['id'] ."'",
        );

        // look for the listing and cache if needed
        $search = Cache::read('listing_view_search-'. $request['id'], 'longterm');
        if (!$search) {
            $search = $this->Flex->GetListings($conditions);
            Cache::write('listing_view_search-'. $request['id'], $search, 'longterm');
        }

        // look for the listing extended information and cache if needed
        $data = Cache::read('listing_view_data-'. $request['id'], 'longterm');
        if (!$data) {
            // get information
            $listing = $this->Flex->GetListing($search[0]['Id'], array(
                '_expand' => 'Photos,CustomFieldsExpanded,Supplement',
            ));
            $data = $listing[0];
            Cache::write('listing_view_data-'. $request['id'], $data, 'longterm');
        }

        $this->Listing->saveViewed($data);

        // define custom fields
        $custom_fields = $data['CustomFields'][0]['Main'];

        $details = [];
        foreach ($custom_fields as $key => $value) {
            $keyname = key($value);
            $cleaned[$keyname] = $value[$keyname];
        }

        $details = [
            'description'       => (
                (!empty($cleaned['General Property Description'])) ? $cleaned['General Property Description'] : ''),
            'location'          => (
                (!empty($cleaned['Location, Tax & Legal'])) ? $cleaned['Location, Tax & Legal'] : ''),
            'rooms'             => (
                (!empty($cleaned['Rooms'])) ? $cleaned['Rooms'] : ''),
            'equip'             => (
                (!empty($cleaned['Equip/Appl Included'])) ? $cleaned['Equip/Appl Included'] : ''),
            'master'            => (
                (!empty($cleaned['Master Bedroom/Bath'])) ? $cleaned['Master Bedroom/Bath'] : ''),
            'features'          => (
                (!empty($cleaned['General Property Description'])) ? $cleaned['Interior Features'] : ''),
            'flooring'          => (
                (!empty($cleaned['Flooring'])) ? $cleaned['Flooring'] : ''),
            'utilities'         => (
                (!empty($cleaned['Utilities'])) ? $cleaned['Utilities'] : ''),
            'subdiv_amenities'  => (
                (!empty($cleaned['Subdiv. Amenities'])) ? $cleaned['Subdiv. Amenities'] : ''),
            'cooling'           => (
                (!empty($cleaned['Cooling'])) ? $cleaned['Cooling'] : ''),
        ];

        $search_filter = ($this->Session->read('SearchFilter')) ? $this->Session->read('SearchFilter') : '';
        
        $this->set(compact('data', 'search_filter', 'url', 'details'));
    }

    private function __setSearch($page)
    {

        $filters = array(
            "PropertyClass Eq 'Residential'",
            "StateOrProvince Eq 'FL'",
            "MlsStatus Eq 'Active'"
        );

        $params = '';

        if (isset($this->request->query)) {
            $query = Sanitize::clean($this->request->query);

            if (isset($query['PropertyType']) && !empty($query['PropertyType'])) {
                $filters[] = "PropertyType Eq '". $query['PropertyType'] ."'";
            }

            if (isset($query['min_price']) && !empty($query['min_price'])) {
                $filters[] = "ListPrice Ge ". $query['min_price'];
            }

            if (isset($query['max_price']) && !empty($query['max_price'])) {
                $filters[] = "ListPrice Le ". $query['max_price'];
            }

            if (isset($query['BedsTotal']) && !empty($query['BedsTotal'])) {
                $filters[] = "BedsTotal Ge ". $query['BedsTotal'];
            }

            if (isset($query['BathsTotal']) && !empty($query['BathsTotal'])) {
                $filters[] = "BathsTotal Ge ". $query['BathsTotal'];
            }

            if (isset($query['City']) && !empty($query['City'])) {
                $filters[] = "City Eq '". ucwords(strtolower($query['City'])) ."'";
            }

            if (isset($query['CountyOrParish']) && !empty($query['CountyOrParish'])) {
                $filters[] = "CountyOrParish Eq '". $query['CountyOrParish'] ."'";
            }

            if (isset($query['PostalCode']) && !empty($query['PostalCode'])) {
                $filters[] = "PostalCode Eq '". $query['PostalCode'] ."'";
            }

            $params_full = [];
            foreach ($query as $key => $value) {
                $params_full[] = '&'. $key .'='. $value;
            }
            $params = '?'. substr(implode('', $params_full), 1);


            $this->Session->write('SearchFilter', $params);
        }

        $search_filter = implode(' And ', $filters);

        $conditions = array(
            '_limit' => 12,
            '_pagination' => 1,
            '_orderby' => '+ListPrice',
            '_page' => $page,
            '_expand' => 'Photos',
            '_filter' => $search_filter
        );

        $this->set(compact('params'));
        return $conditions;
    }
}
