<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('CakeEmail', 'Network/Email');
class ContactsController extends AppController {

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('submit', 'view');
  }

  public function beforeRender () {
    parent::beforeRender();
  }

  public function submit () {
    if ($this->request->data) {
      $data = Sanitize::clean($this->request->data);
      if (isset($data['Contact'])) {
        if ($this->Contact->save($data['Contact'])) {
          self::__sendEmail ($data);
          $this->Session->setFlash('<div class="alert alert-success">We have recieved your contact request and one of our agents will contact you shortly.</div>');
          $this->redirect($this->referer(), null, false);
        }
        else {
          $this->Session->setFlash('<div class="alert alert-error">Sorry we could not process your contact request.</div>');
          $this->data = $data;
          $this->render('contact');
        }
      }
    }
  }

  public function view () {
    if ($this->request->params['slug']) {
      $slug = Sanitize::clean($this->request->params['slug']);
      if (!empty($slug)) {
        $this->render($slug);
      }
      else {
        $this->Session->setFlash('<div class="alert alert-error">Sorry we could not process your contact request.</div>');
        $this->redirect($this->referer(), null, false);
      }
    }
    else {
      $this->Session->setFlash('<div class="alert alert-error">Sorry we could not process your contact request.</div>');
      $this->redirect($this->referer(), null, false);
    }
  }

  private function __sendEmail ($data) {

    unset ($data['Contact']['ip_address']);

    $message = '';
    foreach ($data['Contact'] as $key => $value) {

      $key = str_replace('_', ' ', $key);
      $key = ucwords(strtolower($key));

      $message .= $key .': '. $value ."\r\n";
    }

    $Email = new CakeEmail('mandrill');
    $Email->from(array('noreply@homerunrealestate.com'));
    $Email->to('iamjonbradley@gmail.com');
    $Email->subject($data['Contact']['title']);
    $Email->send($message);
  }

}