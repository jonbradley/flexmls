<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
class PostsController extends AppController {

  public $components = array('Paginator');

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('index', 'view');
  }

  public function beforeRender () {
    parent::beforeRender();
  }

  public function index () {
    $this->Paginator->settings = array(
      'Post' => array(
        'order' => array('id' => 'DESC'),
        'limit' => 4
      )
    );
    $data = $this->Paginator->paginate('Post');
    $this->set(compact('data'));
  }

  public function view () {
    $slug = Sanitize::clean($this->request->params['slug']);

    if (!$slug) {
      $this->Session->setFlash('Invalid Article', 'default', array('class' => 'alert alert-warning'));
      $this->redirect('index', null, false);
    }
    
    $data = $this->Post->find('first', array(
      'conditions' => array(
        'slug' => $slug
      )
    ));

    if ($data) {
      $neighbors = $this->Post->find('neighbors', array(
        'conditions' => array('Post.id' => $data['Post']['id'])
      ));
      $this->set(compact('data','neighbors'));
    }
    else {
      $this->Session->setFlash('Invalid Article', 'default', array('class' => 'alert alert-warning'));
      $this->redirect('index', null, false);
    }

  }

  //---------------------------------
  //      START ADMIN SECTION    
  //---------------------------------

  public function admin_index ($manufacturer = null) {
    parent::isAuthorized();
    $this->Paginator->settings = array(
      'Post' => array(
        'order' => array('id' => 'DESC'),
        'limit' => 10
      )
    );
    $data = $this->Paginator->paginate('Post');
    $this->set(compact('data'));
  }

  public function admin_add() {
    parent::isAuthorized();
    if ($this->request->is('post')) {
      $this->Post->create();
      if ($this->Post->save($this->request->data)) {
        $this->Session->setFlash(__('The Post has been saved'));
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('The Post could not be saved. Please, try again.'));
      }
    }
    $this->render('admin_edit');
  }

  public function admin_edit($id = null) {
    parent::isAuthorized();
    $this->Post->id = $id;
    if (!$this->Post->exists()) {
      throw new NotFoundException(__('Invalid Post'));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->Post->save($this->request->data)) {
        $this->Session->setFlash(__('The Post has been saved'));
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('The Post could not be saved. Please, try again.'));
      }
    } else {
      $this->request->data = $this->Post->read(null, $id);
      unset($this->request->data['Post']['password']);
    }
  }

  public function admin_delete($id) {
    parent::isAuthorized();
    $this->Post->id = $id;
    if (!$this->Post->exists()) {
      throw new NotFoundException(__('Invalid Post'));
    }
    else {
      $this->Post->delete($id, true);
      $this->Session->setFlash(__('The Post has been saved'));
      $this->redirect(array('action' => 'index'));
    }
  }

}