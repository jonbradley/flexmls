<?php

App::import('Vendor', 'Zillow', array('file' => 'Zillow.php'));
class ZillowComponent extends Component {

  public $zws_id = '';

  public function __construct(ComponentCollection $collection, $settings = array()) {

  }

  public function initialize (Controller $controller) {

  }

  public function startup(Controller $controller) {
    $this->Zillow = new Zillow($this->zws_id); // $zws_id is your Zillow API Key
  }

  public function comps ($data) {
    $search_result = $this->Zillow->GetSearchResults(array('address' => $data['Listing']['street_address'], 'citystatezip' => $data['Listing']['city_name']));
    debug ($search_result);
    die;
    $comps = $this->Zillow->GetComps(array('count' => '10', 'rentzestimate' => true, 'zpid' => ''));
  }

  public function estimate ($data) {
    return $this->Zillow->GetZestimate();
  }

  public function charts ($data) {
    return $this->Zillow->GetChart(array('unit-type' => 'dollar'));
  }

}