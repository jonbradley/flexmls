<?php

App::uses('AppController', 'Controller');
App::uses('Flex', 'Vendor');
class SparkCacheComponent extends Component {

  public $tokens = array(
    'FlexMLS' => array(
      'key' => 'fl_o303601_key_1',
      'secret' => 'OsuBBX2Ub0py0AXc6Neqg'
    )
  );

  public function __construct(ComponentCollection $collection, $settings = array()) {

  }

  public function initialize (Controller $controller) {

  }

  public function startup(Controller $controller) {
  }

  public function readCache ($name) {
    $cache = Cache::read($name);
    return $cache;
  }

  public function filter_list () {
    self::__writeCache('City');
    self::__writeCache('CountyOrParish');
    self::__writeCache('PostalCode');
  }

  private function __writeCache($type) {
    $this->Flex = new Flex($this->tokens['FlexMLS']['key'], $this->tokens['FlexMLS']['secret']);
    $this->Flex->setApplicationName("RealEstateDiamond2/1.0");
    $results = $this->Flex->GetStandardFields($type);
    $data = $results[$type]['FieldList'];
    Cache::write($type, $data);
  }

}