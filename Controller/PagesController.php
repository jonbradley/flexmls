<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
class PagesController extends AppController {

	public $name = 'Pages';
	public $uses = array();

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('view');
  }

  public function beforeRender () {
    if (isset($this->request->params['slug']))
      $this->page_name = $this->request->params['slug'];
    parent::beforeRender();
  }

	public function view () {
		$slug = Sanitize::clean($this->request->params['slug']);
		if (!empty($slug)) {
			$data = $this->Page->find('first', array(
				'conditions' => array('Page.slug' => $slug)
			));
			$this->set(compact('data', 'slug'));
		}
		else {
			$this->redirect('/', null, false);
		}
	}

  public function admin_index ($manufacturer = null) {
    parent::isAuthorized();
    $data = $this->paginate('Page');
    $this->set(compact('data'));
  }

  public function admin_add() {
    parent::isAuthorized();
    if ($this->request->is('post')) {
      $this->Page->create();
      if ($this->Page->save($this->request->data)) {
        $this->Session->setFlash(__('The Page has been saved'));
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('The Page could not be saved. Please, try again.'));
      }
    }
    $this->render('admin_edit');
  }

  public function admin_edit($id = null) {
    parent::isAuthorized();
    $this->Page->id = $id;
    if (!$this->Page->exists()) {
      throw new NotFoundException(__('Invalid Page'));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->Page->save($this->request->data)) {
        $this->Session->setFlash(__('The Page has been saved'));
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('The Page could not be saved. Please, try again.'));
      }
    } else {
      $this->request->data = $this->Page->read(null, $id);
      unset($this->request->data['Page']['password']);
    }
  }

  public function admin_delete($id) {
    parent::isAuthorized();
    $this->Page->id = $id;
    if (!$this->Page->exists()) {
      throw new NotFoundException(__('Invalid Page'));
    }
    else {
      $this->Page->delete($id, true);
      $this->Session->setFlash(__('The Page has been saved'));
      $this->redirect(array('action' => 'index'));
    }
  }
  
}
