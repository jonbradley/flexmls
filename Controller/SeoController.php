<?php

class SeoController extends AppController {

  public $uses = array('Seo');

  public function beforeFilter() {
    parent::beforeFilter();
  }

  public function admin_index ($manufacturer = null) {
    parent::isAuthorized();
    $data = $this->paginate('Seo');
    $this->set(compact('data'));
  }

  public function admin_add() {
    parent::isAuthorized();
    if ($this->request->is('post')) {
      $this->Seo->create();
      if ($this->Seo->save(Sanitize::clean($this->request->data))) {
        $this->Session->setFlash(__('The Seo has been saved'));
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('The Seo could not be saved. Please, try again.'));
      }
    }
    $this->render('admin_edit');
  }

  public function admin_edit($id = null) {
    parent::isAuthorized();
    $this->Seo->id = $id;
    if (!$this->Seo->exists()) {
      throw new NotFoundException(__('Invalid Seo'));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->Seo->save(Sanitize::clean($this->request->data))) {
        $this->Session->setFlash(__('The Seo has been saved'));
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('The Seo could not be saved. Please, try again.'));
      }
    } else {
      $this->request->data = $this->Seo->read(null, $id);
      unset($this->request->data['Seo']['password']);
    }
  }

  public function admin_delete($id) {
    parent::isAuthorized();
    $this->Seo->id = $id;
    if (!$this->Seo->exists()) {
      throw new NotFoundException(__('Invalid Seo'));
    }
    else {
      $this->Seo->delete($id, true);
      $this->Session->setFlash(__('The Seo has been saved'));
      $this->redirect(array('action' => 'index'));
    }
  }

}