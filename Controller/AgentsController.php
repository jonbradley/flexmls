<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('File', 'Utility');
class AgentsController extends AppController {

  public $page_name = 'agents';

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('index', 'view');
  }

  public function beforeRender () {
    parent::beforeRender();
  }

  public function index () {
    $data = $this->Agent->find('all', array(
      'conditions' => array(
        'Agent.bio !=""'
      ),
      'order' => array(
        'Agent.lastname', 'Agent.firstname'
      )
    ));
    $this->set(compact('data'));
  }

  public function view () {
    $slug = Sanitize::clean($this->request->params['slug']);
    if (!empty($slug)) {
      $data = $this->Agent->find('first', array(
        'conditions' => array('Agent.slug' => $slug)
      ));

      $listings = ClassRegistry::init('Listing')->getOurRecent($this->Flex, 12);
      $this->set(compact('data', 'slug', 'listings'));
    }
    else {
      $this->redirect('/', null, false);
    }
  }

  public function admin_index() {
    parent::isAuthorized();
    $this->Agent->recursive = 0;
    $this->set('data', $this->paginate());
  }

  public function admin_add() {
    parent::isAuthorized();
    if ($this->request->is('post')) {
      $this->Agent->create();
      if ($this->Agent->save($this->request->data)) {
        self::uploadImage ($this->request->data, $this->Agent->id);
        $this->Session->setFlash(__('The Agent has been saved'));
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('The Agent could not be saved. Please, try again.'));
      }
    }
    $this->render('admin_edit');
  }

  public function admin_edit($id = null) {
    parent::isAuthorized();
    $this->Agent->id = $id;
    if (!$this->Agent->exists()) {
      throw new NotFoundException(__('Invalid Agent'));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->Agent->save($this->request->data)) {
        if (!empty($this->request->data['Agent']['image']['type']))
          self::uploadImage ($this->request->data, $this->Agent->id);
        $this->Session->setFlash(__('The Agent has been saved'));
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('The Agent could not be saved. Please, try again.'));
      }
    } else {
      $this->request->data = $this->Agent->read(null, $id);
    }
  }

  private function uploadImage ($data, $id) {
    if (!empty($data['Agent']['image'])) {

      switch ($data['Agent']['image']['type']) {
        case 'image/jpeg':
          $extension = '.jpg';
          break;
        case 'image/gif':
          $extension = '.gif';
          break;
        case 'image/png':
          $extension = '.png';
          break;
      }

      $image = WWW_ROOT .'img'. DS .'agents'. DS . $id . $extension;

      copy ($data['Agent']['image']['tmp_name'], $image);

      $data['Agent']['photo'] =  $id . $extension;

      unset ($data['Agent']['image']);
      $this->Agent->save($data['Agent'], false);
      
    }
  }

  public function admin_delete($id = null) {
    parent::isAuthorized();
    if (!$this->request->is('post')) {
      throw new MethodNotAllowedException();
    }
    $this->Agent->id = $id;
    if (!$this->Agent->exists()) {
      throw new NotFoundException(__('Invalid Agent'));
    }
    if ($this->Agent->delete()) {
      $this->Session->setFlash(__('Agent deleted'));
      $this->redirect(array('action' => 'index'));
    }
    $this->Session->setFlash(__('Agent was not deleted'));
    $this->redirect(array('action' => 'index'));
  }

}