<?php

class FeaturedListingsController extends AppController {


  public function admin_index ($manufacturer = null) {
    parent::isAuthorized();
    $data = $this->paginate('FeaturedListing');
    $this->set(compact('data'));
  }

  public function admin_add() {
    parent::isAuthorized();
    if ($this->request->is('post')) {
      $this->FeaturedListing->create();
      if ($this->FeaturedListing->save($this->request->data)) {
        $this->Session->setFlash(__('The Featured Listing has been saved'));
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('The Featured Listing could not be saved. Please, try again.'));
      }
    }
    $this->render('admin_edit');
  }

  public function admin_edit($id = null) {
    parent::isAuthorized();
    $this->FeaturedListing->id = $id;
    if (!$this->FeaturedListing->exists()) {
      throw new NotFoundException(__('Invalid Featured Listing'));
    }
    if ($this->request->is('post') || $this->request->is('put')) {
      if ($this->FeaturedListing->save($this->request->data)) {
        $this->Session->setFlash(__('The Featured Listing has been saved'));
        $this->redirect(array('action' => 'index'));
      } else {
        $this->Session->setFlash(__('The Featured Listing could not be saved. Please, try again.'));
      }
    } else {
      $this->request->data = $this->FeaturedListing->read(null, $id);
      unset($this->request->data['FeaturedListing']['password']);
    }
  }

  public function admin_delete($id) {
    parent::isAuthorized();
    $this->FeaturedListing->id = $id;
    if (!$this->FeaturedListing->exists()) {
      throw new NotFoundException(__('Invalid Featured Listing'));
    }
    else {
      $this->FeaturedListing->delete($id, true);
      $this->Session->setFlash(__('The Featured Listing has been saved'));
      $this->redirect(array('action' => 'index'));
    }
  }

}


