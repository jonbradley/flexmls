
<div class="container">
  <div class="row">
    <?php echo $this->element('sidebar', array('url' => '/search_ours')); ?>
    <div id="main" class="span9">

    <?php if (!empty($data['paginate']['TotalRows'])): ?>
    <div class="row-fluid">
      <div class="span4"><h1 class="page-header">Our Properties (<strong><?php echo $data['paginate']['TotalRows']; ?></strong>)</h1></div>
      <div class="span8">
            <div class="filter-wrapper">
                <div class="filter pull-right">
                  <?php echo $this->element('../Listings/_pagination_top'); ?>
                </div>
            </div>
        
      </div>
    </div>
    <?php endif; ?>

    

    <div class="clearfix">


<div class="row-fluid">
      <div class="properties-grid featured">
        <div class="row-fluid">
          <?php 
          if (!empty($data['data'])) {
            $i = 1;
            foreach ($data['data'] as $key => $value) {
              echo $this->element('../Listings/_listing_large', array('data' => $value, 'i' => $i),
                array('cache' => array('config' => 'longterm', 'key' => 'search)ours-'. $value['StandardFields']['ListingId']))
              );
              $i++; 
            }
          }
          ?>
        </div>
    </div>
    <!-- /.row -->
<?php echo $this->element('../Listings/_pagination_bottom'); ?>
</div>
<!-- /.property -->
</div>
<!-- /.row -->



</div>

<!-- /#main -->

</div>
<!-- /.row -->
</div>
<!-- /.container -->

</div>