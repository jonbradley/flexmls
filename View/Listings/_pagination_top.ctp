
<div class="pager pull-right">
    <ul class="pager">
  <?php
  if (!empty($data['paginate']) && !empty($data['paginate']['TotalPages']) && $data['paginate']['TotalPages'] != 1) {

    if (!empty($data['paginate']['TotalPages']) && $data['paginate']['CurrentPage'] != 1) {
      $previous = (int) $data['paginate']['CurrentPage'] - 1;
      echo '<li>'. $this->Html->link('<i class="icon icon-normal-left-arrow-small"></i>', '/search/'. $previous . $params , array('escape' => false)) .'</li>';
    }
    
    if (!empty($data['paginate']['TotalPages']) && $data['paginate']['CurrentPage'] != $data['paginate']['TotalPages']) {
      echo '<li>'. $this->Html->link('<i class="icon icon-normal-right-arrow-small"></i>', '/search/' . $data['paginate']['TotalPages'] . $params, array('escape' => false)); 
    }
  }
  
  ?>
  </ul>
</div>