<div class="sidebar span3">
  <div id="enquireproperties_widget-2" class="widget enquire">

  <h2>Enquire Now</h2>

  <div class="content">
      <?php 
      echo $this->Session->flash(); 
      echo $this->Form->create('Contact', array('url' => '/contacts/submit')); 
      echo $this->Form->hidden('title', array('value' => 'Request a Viewing'));
      echo $this->Form->hidden('ip_address', array('value' => $_SERVER['REMOTE_ADDR']));
      echo $this->Form->hidden('url', array('value' => 'http://'. $_SERVER['HTTP_HOST'] . $this->link->generateLink($data)));
      echo $this->Form->hidden('address', array('value' => $data['StandardFields']['UnparsedAddress']));
      echo $this->Form->hidden('MLS', array('value' => $data['Id']));
      ?>
      <div class="control-group">
        <label class="control-label" for="inputName">
          Name
          <span class="form-required" title="This field is required.">*</span>
        </label>

        <?php 
        echo $this->Form->input('name', array(
          'label' => false, 
          'div' => 'controls',
          'type' => 'text',
        ));
        ?>
        <!-- /.controls -->
      </div><!-- /.control-group -->

      <div class="control-group">
        <label class="control-label" for="inputPhone">
          Phone
          <span class="form-required" title="This field is required.">*</span>
        </label>

        <?php 
        echo $this->Form->input('phone_number', array(
          'label' => false, 
          'div' => 'controls',
          'type' => 'text',
        ));
        ?>
        <!-- /.controls -->
      </div><!-- /.control-group -->

      <div class="control-group">
        <label class="control-label" for="inputDate">
          Best Time to Contact
          <span class="form-required" title="This field is required.">*</span>
        </label>

        <?php 
        echo $this->Form->input('best_time_to_contact', array(
          'label' => false, 
          'div' => 'controls',
          'type' => 'text',
        ));
        ?>
        <!-- /.controls -->
      </div><!-- /.control-group -->

      <div class="control-group">
        <label class="control-label" for="inputEmail">
          Email
          <span class="form-required" title="This field is required.">*</span>
        </label>


        <?php 
        echo $this->Form->input('email', array(
          'label' => false, 
          'div' => 'controls',
          'type' => 'text',
        ));
        ?>
        <!-- /.controls -->
      </div><!-- /.control-group -->

      <div class="form-actions">
        <input type="hidden" name="post_id" value="358">
        <button class="btn btn-primary arrow-right">Send</button>
      </div>
      <!-- /.form-actions -->
    </form>
  </div><!-- /.content -->

</div>

  <h2>Search Properties</h2>

  <?php  echo $this->element('search', array('url' => $url)); ?>
</div>