
<div id="search-view">
  <div class="container">
    <div class="row breadcrumb">
      <?php echo $this->Html->link('Search Results', '/search'. $this->Session->read('SearchFilter'));  ?>
      &raquo;
      <?php
      $county = '?CountyOrParish='. $data[0]['StandardFields']['CountyOrParish'];
      $city   = '&City='.           $data[0]['StandardFields']['City'];
      $mls    = '&MlsId='.          $data[0]['Id'];
      ?>
      <?php echo $this->Html->link($data[0]['StandardFields']['CountyOrParish'], '/search'. $county);  ?>
      &raquo;
      <?php echo $this->Html->link($data[0]['StandardFields']['City'], '/search'. $county . $city);  ?>
      &raquo;
      MLS: 
      <?php echo $data[0]['Id'];  ?>
    </div>
    <?php echo $this->Session->flash(); ?>
    <div class="row-fluid">
      <div class="span4 border-right">
        <?php
        $address  = $data[0]['StandardFields']['StreetNumber'] .' ';
        $address .= $data[0]['StandardFields']['StreetName'] .' ';
        $address .= $data[0]['StandardFields']['StreetSuffix'];
        echo '<h3><strong>'. $address .'</strong></h3>';
        echo $data[0]['StandardFields']['City'] .'<br />';
        echo '<span class="small">'. ucwords(strtolower($data[0]['StandardFields']['SubdivisionName'])) .'</span>';
        ?>
        


      </div>
      <div class="span4 border-right align-center">
        <h3><?php echo $this->Number->currency($data[0]['StandardFields']['ListPrice'], 'USD'); ?></h3>
        <button type="button" data-toggle="modal" data-target="#myModal" id="modal" class="btn btn-info" onclick="jQuery('#myModal').modal()">Request a Viewing</button>
        <br />
      </div>
      <div class="span4">
        <strong><?php echo $data[0]['StandardFields']['PropertySubType']; ?></strong> <br />

        <?php if ($data[0]['StandardFields']['PropertyClass'] == 'Residential') { ?>
          <strong><?php echo $data[0]['StandardFields']['BedsTotal']; ?></strong> Beds &nbsp; 
          <strong>
            <?php 
            echo $data[0]['StandardFields']['BathsFull']; 
            if (!empty($data[0]['StandardFields']['BathsHalf'])) echo '/'. $data[0]['StandardFields']['BathsHalf'];
            ?>
          </strong> Baths &nbsp; 
        <?php } ?>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="row-fluid">
      <div class="span8">
        <div id="myCarousel" class="carousel slide">
          <!-- Indicators -->

          <ol class="carousel-indicators">
            <?php
            if (!empty($data[0]['StandardFields']['Photos']['0'])) echo '<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['1'])) echo '<li data-target="#carousel-example-generic" data-slide-to="1"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['2'])) echo '<li data-target="#carousel-example-generic" data-slide-to="2"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['3'])) echo '<li data-target="#carousel-example-generic" data-slide-to="3"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['4'])) echo '<li data-target="#carousel-example-generic" data-slide-to="4"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['5'])) echo '<li data-target="#carousel-example-generic" data-slide-to="5"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['6'])) echo '<li data-target="#carousel-example-generic" data-slide-to="6"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['7'])) echo '<li data-target="#carousel-example-generic" data-slide-to="7"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['8'])) echo '<li data-target="#carousel-example-generic" data-slide-to="8"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['9'])) echo '<li data-target="#carousel-example-generic" data-slide-to="9"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['10'])) echo '<li data-target="#carousel-example-generic" data-slide-to="10"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['11'])) echo '<li data-target="#carousel-example-generic" data-slide-to="11"></li>';
            if (!empty($data[0]['StandardFields']['Photos']['12'])) echo '<li data-target="#carousel-example-generic" data-slide-to="12"></li>';
            ?>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <?php
            if (!empty($data[0]['StandardFields']['Photos']['0'])) echo '<div class="item active"><img src="'. $data[0]['StandardFields']['Photos']['0']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['1'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['1']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['2'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['2']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['3'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['3']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['4'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['4']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['5'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['5']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['6'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['6']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['7'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['7']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['8'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['8']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['9'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['9']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['10'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['10']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['11'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['11']['UriLarge'] .'" width="620" /></div>';
            if (!empty($data[0]['StandardFields']['Photos']['12'])) echo '<div class="item"><img src="'. $data[0]['StandardFields']['Photos']['12']['UriLarge'] .'" width="620" /></div>';
            ?>
          </div>
          <!-- Controls -->
          <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
          <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>
      </div>
      <div class="span4 map">
        <?php
          // init map (prints container)
          echo $this->GoogleMap->map(array(
            'div' => array(
              'height' => '465', 
              'width' => '106%'
            ),
            'map' => array(
              'defaultLat' => '"'. $data[0]['StandardFields']['Latitude'] .'"',
              'defaultLng' => '"'. $data[0]['StandardFields']['Longitude'] .'"',
              'defaultZoom' => 17
            )
          ));
           
          // add markers
          $options = array(
              'lat' => '"'. $data[0]['StandardFields']['Latitude'] .'"',
              'lng' => '"'. $data[0]['StandardFields']['Longitude'] .'"'
          );
          // tip: use it inside a for loop for multiple markers
          $this->GoogleMap->addMarker($options);
           
          // print js
          echo $this->GoogleMap->script();
        ?>
      </div>


    </div>

    <div class="row-fluid details">
      <div class="span4 essentials">
        <h4>Essentials</h4>
        Price: <?php echo $this->Number->currency($data[0]['StandardFields']['ListPrice'], 'USD'); ?> <br />
        <?php if (!empty($data[0]['StandardFields']['PropertySubType'])) { ?> Type: <?php echo $data[0]['StandardFields']['PropertySubType']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['BedsTotal'])) { ?> Bedrooms: <?php echo $data[0]['StandardFields']['BedsTotal']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['BathsFull'])) { ?> Bathrooms: <?php echo $data[0]['StandardFields']['BathsFull']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['RoomsTotal'])) { ?> Rooms: <?php echo $data[0]['StandardFields']['RoomsTotal']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['BuildingAreaTotal'])) { ?> Approx. Sq.: <?php echo $data[0]['StandardFields']['BuildingAreaTotal']; ?> <br /> <?php } ?>
      </div>
      <div class="span4 essentials">
        <h4>Address</h4>
        <?php if (!empty($data[0]['StandardFields']['SubdivisionName'])) { ?> Sub-Division: <?php echo ucwords(strtolower($data[0]['StandardFields']['SubdivisionName'])); ?> <br /> <?php } ?>
        Street Address: <?php echo $data[0]['StandardFields']['StreetNumber'] .' '. $data[0]['StandardFields']['StreetName'] .' '. $data[0]['StandardFields']['StreetSuffix']; ?> <br /> 
        <?php if (!empty($data[0]['StandardFields']['UnitNumber'])) { ?> Unit #: <?php echo $data[0]['StandardFields']['UnitNumber']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['City'])) { ?> City: <?php echo $data[0]['StandardFields']['City']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['StateOrProvince'])) { ?> State: <?php echo $data[0]['StandardFields']['StateOrProvince']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['CountyOrParish'])) { ?> County: <?php echo $data[0]['StandardFields']['CountyOrParish']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['PostalCode'])) { ?> Zip Code: <?php echo $data[0]['StandardFields']['PostalCode']; ?> <br /> <?php } ?>
      </div>
      <div class="span4 essentials">
        <h4>Realtor Information</h4>
        <?php echo $data['0']['StandardFields']['ListOfficeName']; ?>
        <br />
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Request a Viewing</h3>
          </div>
          <div class="modal-body">
            <?php echo $this->element('../Contacts/request-a-viewing'); ?>
            <br />
          </div>
        </div>
      </div>
    </div>

    <div class="row-fluid comments">
      <?php echo ucwords(strtolower($data[0]['StandardFields']['PublicRemarks'])) ?>
    </div>

    <!-- begin IDX copyright -->
    <div class="rowfluid disclaimer center muted comments">
      <div class="pull-left"><img src="http://cdn.photos.sparkplatform.com/fl/20130326145114154483000000.jpg" class="pull-left"></div>
      <div class="disclaimer-text">
        This information is not verified for authenticity or accuracy and is not guaranteed and may not reflect all real estate activity in the market. 
        Copyright <script>var d=new Date(); document.write( d.getFullYear());</script> Regional Multiple Listing Service, Inc. All rights reserved. 
        Information Not Guaranteed and Must Be Confirmed by End User. Site contains live data.
      </div>
    </div>

  </div>
</div>