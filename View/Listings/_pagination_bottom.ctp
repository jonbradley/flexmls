<div class="pagination pagination-centered">
  <ul class="unstyled">
  <?php
  if (!empty($data)) {
    $total_pages = $data['paginate']['TotalPages'];
    $total_results = $data['paginate']['TotalRows'];
    $current_page = $data['paginate']['CurrentPage'];
    $items_per_page = 12;
    $max_links = 5;
    $next = (int) $current_page + 1;
    $previous = (int) $current_page - 1;
    $start = 0;
    $end = 0;

    $current_url = '/'. $this->params['action'];
    
    if ($total_pages != 1) {

      if ($current_page != 1) {
        echo '<li>'. $this->Html->link('First', $current_url .'/1' . $params , array('escape' => false)) .'</li>';
        echo '<li>'. $this->Html->link('Previous', $current_url .'/'. $previous . $params , array('escape' => false)) .'</li>';
      }

      // if less then $max_links pages
      if ($total_pages <= $max_links) {
        $start = 1;
        $end = $total_pages;
      }
      // if more then $max_links pages
      if ($total_pages > $max_links) {
        $start = $current_page;
        $end = $current_page + $max_links;
      }
      // if current page is less the $max_links from end
      if ($current_page + $max_links >= $total_pages) {
        $remaining_pages =  $max_links - ($total_pages - $current_page);
        $start = $current_page - ($remaining_pages - 1);
        $end = $total_pages;
      }
      // if on the last page
      if ($total_pages == $current_page) {
        $start = $current_page - $max_links;
        $end = $total_pages;
      }
      // if in $max_links to the last page
      if ($current_page == 1) {
        $start = 1;
        if ($total_pages <= $max_links) 
          $end = $total_pages;
        else 
          $end = $current_page + 5;

      }

      for ($i = $start; $i <= $end; $i++) {
        if ($current_page == $i) 
          echo '<li class="active">'. $this->Html->link($i, '#') .'</li>';
        else 
          echo '<li>'. $this->Html->link($i, $current_url .'/'. $i . $params) .'</li>';
      }
      
      if ($current_page != $total_pages) {
        echo '<li>'. $this->Html->link('Next', $current_url .'/' . $next . $params, array('escape' => false)); 
        echo '<li>'. $this->Html->link('Last', $current_url .'/' . $total_pages . $params, array('escape' => false)); 
      }
    }  
  }
    
  ?>
  </ul>
</div>