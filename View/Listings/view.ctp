<?php

$address = $this->Link->address($data);

$description = strtolower($data['StandardFields']['PublicRemarks']);
$description = ucfirst($description);
?>
<div class="container">
  <div class="row">
  <?php echo $this->element('../Listings/_sidebar_listing', array('url' => '/search')); ?>
<div id="main" class="span9 single-property">

<h1 class="page-header fl"><?php echo $address; ?></h1>

<div class="property-detail">

<div class="row">
  <div class="span6 gallery">

    <style type="text/css">
      .thumb-detail {
        margin-right: 20px;
        cursor: pointer;
      }
      .thumb-detail img {
        width: 94px;
        height: 70px;
      }
      .main {
        width: 570px;
        height: 425px;
      }
      .thumb-detail:last-child {
        margin: 0;
      }
    </style>

    <img src="<?php echo $data['StandardFields']['Photos']['0']['UriLarge']?>" width="570" height="425" class="main" />
    <br />

    <?php
    if (!empty($data['StandardFields']['Photos']['0'])) 
      echo '<a href="javascript:void(0)" class="thumb-detail" id="thumb-0"><img src="'. $data['StandardFields']['Photos']['0']['UriLarge'] .'" width="94" height="70" /></a>';
    if (!empty($data['StandardFields']['Photos']['1'])) 
      echo '<a href="javascript:void(0)" class="thumb-detail" id="thumb-1"><img src="'. $data['StandardFields']['Photos']['1']['UriLarge'] .'" width="94" height="70" /></a>';
    if (!empty($data['StandardFields']['Photos']['2'])) 
      echo '<a href="javascript:void(0)" class="thumb-detail" id="thumb-2"><img src="'. $data['StandardFields']['Photos']['2']['UriLarge'] .'" width="94" height="70" /></a>';
    if (!empty($data['StandardFields']['Photos']['3'])) 
      echo '<a href="javascript:void(0)" class="thumb-detail" id="thumb-3"><img src="'. $data['StandardFields']['Photos']['3']['UriLarge'] .'" width="94" height="70" /></a>';
    if (!empty($data['StandardFields']['Photos']['4'])) 
      echo '<a href="javascript:void(0)" class="thumb-detail" id="thumb-4"><img src="'. $data['StandardFields']['Photos']['4']['UriLarge'] .'" width="94" height="70" /></a>';
    if (!empty($data['StandardFields']['Photos']['5'])) 
      echo '<a href="javascript:void(0)" class="thumb-detail" id="thumb-5"><img src="'. $data['StandardFields']['Photos']['5']['UriLarge'] .'" width="94" height="70" /></a>';
    if (!empty($data['StandardFields']['Photos']['6'])) 
      echo '<a href="javascript:void(0)" class="thumb-detail" id="thumb-6"><img src="'. $data['StandardFields']['Photos']['6']['UriLarge'] .'" width="94" height="70" /></a>';
    if (!empty($data['StandardFields']['Photos']['7'])) 
      echo '<a href="javascript:void(0)" class="thumb-detail" id="thumb-7"><img src="'. $data['StandardFields']['Photos']['7']['UriLarge'] .'" width="94" height="70" /></a>';
    if (!empty($data['StandardFields']['Photos']['8'])) 
      echo '<a href="javascript:void(0)" class="thumb-detail" id="thumb-8"><img src="'. $data['StandardFields']['Photos']['8']['UriLarge'] .'" width="94" height="70" /></a>';
    if (!empty($data['StandardFields']['Photos']['9'])) 
      echo '<a href="javascript:void(0)" class="thumb-detail" id="thumb-9"><img src="'. $data['StandardFields']['Photos']['9']['UriLarge'] .'" width="94" height="70" /></a>';
    ?>

  </div>

  <div class="overview">
    <div class="pull-right overview">
      <div class="row">
        <div class="span3">
          <!-- <h2>Overview</h2> -->

          <table>
            <tbody>
            
            <tr>
              <th>Price:</th>
              <td class="price">
                <?php echo $this->Number->currency($data['StandardFields']['ListPrice'] ,'USD'); ?>
              </td>
            </tr>


            <?php if (!empty($data['StandardFields']['MajorChangeType'])) { ?>
            <tr>
              <th>Last Update</th>
              <td><?php echo $data['StandardFields']['MajorChangeType']; ?></td>
            </tr>
            <?php } ?>

            <tr>
              <th>ID:</th>
              <td><strong><?php echo $data['StandardFields']['ListingId']; ?></strong></td>
            </tr>

            <tr>
              <th>Class:</th>
              <td><?php echo $data['StandardFields']['PropertyClass']; ?></td>
            </tr>

            <tr>
              <th>Type:</th>
              <td><?php echo $details['description'][0]['Type']; ?></td>
            </tr>

            <tr>
              <th>Year Built:</th>
              <td><?php echo $details['description'][4]['Year Built']; ?></td>
            </tr>

            <tr>
              <th>Bedrooms:</th>
              <td><?php echo $data['StandardFields']['BedsTotal']; ?></td>
            </tr>

            <tr>
              <th>Bathrooms:</th>
              <td><?php echo $data['StandardFields']['BathsTotal']; ?></td>
            </tr>

            <tr>
              <th>Area:</th>
              <td><?php echo $this->Number->format($data['StandardFields']['BuildingAreaTotal']);?>ft<sup>2</sup></td>
            </tr>
            </tbody>
          </table>
          <br />
          <h2>Realtor Information</h2>
          <p>
            <?php echo $data['StandardFields']['ListOfficeName']; ?>
          </p>
        </div>
        <!-- /.span2 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.overview -->    </div>
</div>

<h2>About Property</h2>

<p><?php echo $description; ?></p>


<div class="row-fluid">
  <div class="span12">
    <h2>General amenities</h2>

    <div class="row-fluid">

    <?php 

    $amenities = array_merge($details['utilities'], $details['subdiv_amenities']);
    $amenities = array_merge($amenities, $details['flooring']);
    $amenities = array_merge($amenities, $details['features']);
    $amenities = array_merge($amenities, $details['equip']);
    $amenities = array_merge($amenities, $details['cooling']);

    $total = count($amenities);

    $split = ceil($total/4);
    ?>

      <ul class="span3 amenities">
        <?php 
        $i = 1;
        foreach ($amenities as $key => $value) {

          $name = key($value);
          if ($name == 'Central') $name = 'Central Air';
          echo '<li class="checked">'. $name .'</li>';

          if (($i%$split) == 0) {
            echo '</ul><ul class="span3">';
          } 

          $i++;
        }
        ?>
      </ul>
    </div>
  </div>
</div>

<div class="row-fluid">
  <h2>Map</h2>

  <?php
    // init map (prints container)
    echo $this->GoogleMap->map(array(
    'div' => array(
      'height' => '370', 
      'width' => '100%'
    ),
    'map' => array(
      'defaultLat' => '"'. $data['StandardFields']['Latitude'] .'"',
      'defaultLng' => '"'. $data['StandardFields']['Longitude'] .'"',
      'defaultZoom' => 17
    )
    ));
     
    // add markers
    $options = array(
      'lat' => '"'. $data['StandardFields']['Latitude'] .'"',
      'lng' => '"'. $data['StandardFields']['Longitude'] .'"'
    );
    // tip: use it inside a for loop for multiple markers
    $this->GoogleMap->addMarker($options);
     
    // print js
    echo $this->GoogleMap->script();
  ?>

</div>

  <div class="clearfix row-fluid">&nbsp;</div>

    <!-- begin IDX copyright -->
    <div class="row-fluid disclaimer center muted comments">
      <div class="pull-left" style="padding-right: 10px;">
        <img src="http://cdn.photos.sparkplatform.com/fl/20130326145114154483000000.jpg" class="pull-left">
      </div>
      <div class="disclaimer-text" style="font-size: 90%">
        This information is not verified for authenticity or accuracy and is not guaranteed and may not reflect all real estate activity in the market. 
        Copyright <script>var d=new Date(); document.write( d.getFullYear());</script> Regional Multiple Listing Service, Inc. All rights reserved. 
        Information Not Guaranteed and Must Be Confirmed by End User. Site contains live data.
      </div>
    </div>
</div>

</div>


</div>
</div>