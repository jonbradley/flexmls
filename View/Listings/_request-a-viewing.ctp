<div id="contact">
<?php 
echo $this->Session->flash(); 
echo $this->Form->create('Contact', array('url' => '/contacts/submit')); 
echo $this->Form->hidden('title', array('value' => 'Request a Viewing'));
echo $this->Form->hidden('ip_address', array('value' => $_SERVER['REMOTE_ADDR']));
?>

<br />

<div class="row-fluid">
  <div class="span6">
    <?php
    echo $this->Form->hidden('url', array('value' => $url));
    echo $this->Form->hidden('MLS', array('value' => $data[0]['Id']));
    ?>
    <strong>Address: </strong> <br />
    <?php
        $address  = $data[0]['StandardFields']['StreetNumber'] .' ';
        $address .= $data[0]['StandardFields']['StreetName'] .' ';
        $address .= $data[0]['StandardFields']['StreetSuffix'];
        echo ucwords(strtolower($address)) .'<br />';
        echo $data[0]['StandardFields']['City'] .', '. $data[0]['StandardFields']['StateOrProvince'] .' '. $data[0]['StandardFields']['PostalCode'] .'<br />';
    ?>
    <br />
        <strong>Details: </strong> <br />
        Price: <?php echo $this->Number->currency($data[0]['StandardFields']['ListPrice'], 'USD'); ?> <br />
        <?php if (!empty($data[0]['StandardFields']['PropertySubType'])) { ?> Type: <?php echo $data[0]['StandardFields']['PropertySubType']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['BedsTotal'])) { ?> Bedrooms: <?php echo $data[0]['StandardFields']['BedsTotal']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['BathsFull'])) { ?> Bathrooms: <?php echo $data[0]['StandardFields']['BathsFull']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['RoomsTotal'])) { ?> Rooms: <?php echo $data[0]['StandardFields']['RoomsTotal']; ?> <br /> <?php } ?>
        <?php if (!empty($data[0]['StandardFields']['BuildingAreaTotal'])) { ?> Approx. Sq.: <?php echo $data[0]['StandardFields']['BuildingAreaTotal']; ?> <br /> <?php } ?>
        <br />
        <strong>Realtor: </strong> <br />
        <?php echo $data['0']['StandardFields']['ListOfficeName']; ?>
  </div>
  <div class="span6">
    <?php 
      echo $this->Form->input('name', array(
        'label' => 'Your Name',
        'type' => 'text',
      ));
      echo $this->Form->input('phone_number', array(
        'label' => 'Phone Number',
        'type' => 'text',
      ));
      echo $this->Form->input('email', array(
        'label' => 'Email Address',
        'type' => 'text',
      ));
      echo $this->Form->input('best_time_to_contact', array(
        'label' => 'Best Time to Contact',
        'type' => 'text',
      ));
    ?>
  </div>
</div>

<br />

<div class="row-fluid">
  <div class="span6">
  </div>
  <div class="span6">
    <?php 
      echo $this->Form->submit('Submit Viewing Request', array('class' => 'btn btn-info btn'));
    ?>
  </div>
</div>

<?php

echo $this->Form->end();
?>
</div>