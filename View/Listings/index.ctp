<?php //echo $this->element('slider'); ?>
<div class="container">
  <div class="row">
    <?php echo $this->element('sidebar', array('url' => '/search')); ?>
    <div id="main" class="span9">
      <h2>Featured Properties</h2>
      <div class="properties-grid featured">
        <div class="row">
          <?php 
            foreach ($featured_listings as $key => $value) {
              echo $this->element(
                '../Listings/_featured', 
                array('data' => $value),
                array('cache' => array('config' => 'longterm', 'key' => 'featured_home-'. $value['StandardFields']['ListingId']))
              );
            }
          ?>
        </div>
      </div>
      <div class="show-all">
        <a href="/search">Show all</a>
      </div>            
      <hr />
      <h1 class="page-header">Our Newest Listings</h1>
      <div class="properties-grid">
        <div class="row-fluid">
          <?php 
            $i = 1;
            foreach ($listings as $key => $value) {
              echo $this->element(
                '../Listings/_recent_properties', 
                array('data' => $value, 'i' => $i),
                array('cache' => array('config' => 'longterm', 'key' => 'newest_home-'. $value['StandardFields']['ListingId']))
              );
              $i++; 
            }
          ?>
          </div>
        <div class="row-fluid"></div>
      </div>
      <div class="show-all">
        <a href="/our_most_recent">Show all</a>
      </div>           
    </div>
  </div>
</div>