<?php
$address = $this->Link->address_short($data);

$description = strtolower($data['StandardFields']['PublicRemarks']);
$description = ucfirst($description);
$description = $this->Text->truncate($description, 250);

// set the image
$image = $this->Html->image($data['StandardFields']['Photos'][0]['Uri300'], array('alt' => $address, 'style' => 'width: 270px; height: 200px;'));
?>
<div class="span3">
        <div class="property">
            <div class="image">
                <div class="content">
                    <a href="<?php echo $this->link->generateLink($data); ?> ">
                        <div class="description"><p><?php echo $this->Text->truncate($description, 100); ?> <br /><br />Click to learn more.</p></div>
                        <?php echo $image; ?>
                    </a>
                </div>
                <!-- /.content -->

                <div class="rent-sale">
                    Sale
                </div>
                <!-- /.rent-sale -->

                <div class="price">
                    <?php echo $this->Number->currency($data['StandardFields']['ListPrice'] ,'USD'); ?>
                </div>
                <!-- /.price -->
                
                <?php if (!empty($data['StandardFields']['MajorChangeType'])) { ?>
                <div class="reduced">
                    <?php echo $data['StandardFields']['MajorChangeType']; ?>
                </div>
                <?php } ?>

            </div>
            <!-- /.image -->

            <div class="info">
                <div class="title clearfix">
                    <h2><?php echo $this->link->listing($address, $data); ?></h2>
                </div>
                <!-- /.title -->

                <div class="location"><?php echo $data['StandardFields']['City']; ?></div>
                <!-- /.location -->
            </div>
            <!-- /.info -->

        </div>
        <!-- /.property -->

        <div class="property-info clearfix">
            <?php if (!empty($data['StandardFields']['BuildingAreaTotal']) && $data['StandardFields']['BuildingAreaTotal'] != '********') { ?>
            <div class="area">
                <i class="icon icon-normal-cursor-scale-up"></i>
                <?php echo $this->Number->format($data['StandardFields']['BuildingAreaTotal']);?>ft<sup>2</sup>
            </div>
            <?php } ?>
            <!-- /.area -->

            <?php if (!empty($data['StandardFields']['BedsTotal']) && $data['StandardFields']['BedsTotal'] != '********') { ?>
            <div class="bedrooms">
                <i class="icon icon-normal-bed"></i>
                <?php echo $data['StandardFields']['BedsTotal']; ?>
            </div>
            <?php } ?>
            <!-- /.bedrooms -->

            <?php if (!empty($data['StandardFields']['BathsTotal']) && $data['StandardFields']['BathsTotal'] != '********') { ?>
            <div class="bathrooms">
                <i class="icon icon-normal-shower"></i>
                <?php echo $data['StandardFields']['BathsTotal']; ?>
            </div>
            <?php } ?>
            <!-- /.bathrooms -->
        </div>
        <!-- /.property-info -->

    </div>