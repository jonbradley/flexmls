<h2>Users</h2>

<div class="row right">
  <?php echo $this->Paginator->counter('Page {:page} of {:pages}, showing {:current} records out of {:count} total'); ?>
</div>

<table class="table table-striped table-bordered table-hover">

<tr>
  <th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
  <th><?php echo $this->Paginator->sort('name', 'Username'); ?></th>
  <th class="center">Actions</th>
</tr>

<?php foreach ($data as $key => $value) { ?>
  
  <tr>
    <td valign="middle"><?php echo $value['User']['id']; ?></td>
    <td valign="middle"><?php echo $value['User']['username']; ?></td>
    <td valign="middle" class="center">
      <?php echo $this->Html->link($this->Html->image('icons/edit.png'), array('action' => 'edit', $value['User']['id']), array('escape' => false)); ?> &nbsp;
      <?php echo $this->Html->link($this->Html->image('icons/delete.png'), array('action' => 'delete', $value['User']['id']), array('escape' => false)); ?> 
    </td>
  </tr>

<?php } ?>

</table>

<div class="pagination right row">
  <ul>
  <?php 
  echo $this->Paginator->numbers(array('tag' => 'li', 'before' => false, 'after' => false, 'separator' => false, 'currentClass' => 'active')); 
  ?>
  </ul>
</div>