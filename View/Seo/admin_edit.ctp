<h2>Edit Page</h2>
<div class="users form">
<?php echo $this->Form->create('Seo'); ?>
    <fieldset>
        <?php 
        echo $this->Form->input('name', array('style' => 'width: 100%;', 'disabled' => true));
        echo $this->Form->input('keywords', array('style' => 'width: 100%;'));
        echo $this->Form->input('description', array('style' => 'width: 100%; height: 200px'));
    ?>
    </fieldset>
<?php 
echo $this->Form->submit('Submit', array('class' => 'btn')); 
echo $this->Form->end();
?>
</div>