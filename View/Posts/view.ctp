<div class="container">
  <div class="row">
    <?php echo $this->element('sidebar', array('url' => '/search')); ?>
    <div id="main" class="span9">

      <article id="post-<?php echo $data['Post']['id']; ?>" class="clearfix post-<?php echo $data['Post']['id']; ?> page type-page status-publish entry ">

        <div class="row-fluid">
          <div class="span9"><h1 class="page-header"><?php echo $data['Post']['title']; ?></h1></div>
          <div class="span3">
            <div class="filter-wrapper">
              <div class="filter pull-right">
                <div class="pager pull-right">
                  <ul class="pager">
                    <?php if ($neighbors['next'] != null) { ?><li><a href="/blog/<?php echo $neighbors['next']['Post']['slug']; ?>"><i class="icon icon-normal-left-arrow-small"></i></a></li><?php } ?>
                    <?php if ($neighbors['prev'] != null) { ?><li><a href="/blog/<?php echo $neighbors['prev']['Post']['slug']; ?>"><i class="icon icon-normal-right-arrow-small"></i></a></li><?php } ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <header class="entry-header">
          <div class="meta">
            Posted on <?php echo $this->Time->format('F d', $data['Post']['created']); ?>
          </div>
        </header>

        <div class="entry-content">
          <?php 
          echo $data['Post']['body'];
          ?>
        </div>

      </article>

    </div>
  </div>
</div>