
<div class="container">
  <div class="row">
    <?php echo $this->element('sidebar', array('url' => '/search')); ?>
    <div id="main" class="span9">

    <div class="row-fluid">
      <div class="span4"><h1 class="page-header">Latest Blog Posts</h1></div>
      <div class="span8">
        <div class="filter-wrapper">
          <div class="filter pull-right">
            <div class="pager pull-right">
              <ul class="pager">
                <?php
                echo '<li>'. $this->Paginator->next('<i class="icon icon-normal-left-arrow-small"></i>', array('escape' => false, 'tag' => false), null, array('class' => 'disabled')) .'</li>';
                echo '<li>'. $this->Paginator->prev('<i class="icon icon-normal-right-arrow-small"></i>', array('escape' => false, 'tag' => false), null, array('class' => 'disabled')) .'</li>';
                ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    

    <div class="clearfix">
      <?php foreach ($data as $key => $value) { ?>
        <div class="row-fluid article-list">
          <article id="post-<?php echo $value['Post']['id']; ?>" class="clearfix post-<?php echo $value['Post']['id']; ?> page type-page status-publish entry ">

            <div class="row-fluid">
              <div class="span9">
                <a href="/blog/<?php echo $value['Post']['slug']; ?>"><h1 class="page-header"><?php echo $value['Post']['title']; ?></h1></a>
              </div>
            </div>

            <header class="entry-header">
              <div class="meta">
                Posted on <?php echo $this->Time->format('F d', $value['Post']['created']); ?>
              </div>
            </header>

            <div class="entry-content">
              <?php echo $value['Post']['body']; ?>
            </div>

            <div class="readmore">
              <a href="/blog/<?php echo $value['Post']['slug']; ?>">Read More &raquo;</a>
            </div>

          </article>
        </div>
      <?php } ?>

      <div class="pagination pagination-centered">
        <ul class="unstyled">
          <?php
          echo $this->Paginator->next('Older',
            array('escape' => false, 'tag' => 'li'),
            null,
            array('class' => 'disabled', 'tag' => 'li')
          );
          echo $this->Paginator->numbers(array(
            'tag' => 'li', 
            'currentClass' => 'active', 
            'modulus' => 8,
            'separator' => '',
            'escape' => false
          ));
          echo $this->Paginator->prev('Newest', 
            array('escape' => false, 'tag' => 'li'),
            null,
            array('class' => 'disabled', 'tag' => 'li')
          );
            
          ?>
        </ul>
      </div>  

    </div>
    <!-- /.row -->
</div>
<!-- /.property -->
</div>
<!-- /.row -->



</div>

<!-- /#main -->

</div>
<!-- /.row -->
</div>
<!-- /.container -->

</div>