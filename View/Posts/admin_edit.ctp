<h2>Edit Page</h2>
<div class="users form">
<?php echo $this->Form->create('Post'); ?>
    <fieldset>
        <?php 
        echo $this->Form->input('Post.title', array('style' => 'width: 100%;'));
        echo $this->Form->input('Post.body', array('style' => 'width: 100%; height: 500px'));
        echo $this->Form->input('Post.slug', array('style' => 'width: 100%;'));
        echo '<hr>';
        echo $this->Form->input('Post.seo_title', array('style' => 'width: 100%;'));
        echo $this->Form->input('Post.seo_meta', array('style' => 'width: 100%;'));
        echo $this->Form->input('Post.seo_description', array('style' => 'width: 100%;'));
        echo '<hr>';
    ?>
    </fieldset>
<?php 
echo $this->Form->submit('Submit', array('class' => 'btn')); 
echo $this->Form->end();
?>
</div>