<?php
$address = $this->Link->address($data);

$description = strtolower($data['StandardFields']['PublicRemarks']);
$description = ucfirst($description);
$description = $this->Text->truncate($description, 250);

// set the image
$image = $this->Html->image($data['StandardFields']['Photos'][0]['Uri300'], array('alt' => $address, 'style' => 'width: 100px; height: 100px;'));
?>
<div class="property clearfix">
  <div class="image">
    <?php echo $this->Link->listing($image, $data); ?>
  </div>
  <div class="wrapper">
    <div class="title">
      <h3><?php echo $this->Link->listing($address, $data); ?></h3>
    </div>
    <div class="location"><?php echo $data['StandardFields']['City']; ?></div>
    <div class="price">
      <?php echo $this->Number->currency($data['StandardFields']['ListPrice'] ,'USD'); ?>
    </div>
  </div>
</div>

<div class="property-info clearfix">
  <div class="area">
    <i class="icon icon-normal-cursor-scale-up"></i>
    <?php echo $this->Number->format($data['StandardFields']['BuildingAreaTotal']);?>ft<sup>2</sup>
  </div>
  <div class="bedrooms">
    <i class="icon icon-normal-bed"></i>
    <?php echo $data['StandardFields']['BedsTotal']; ?>
  </div>
  <div class="bedrooms">
    <i class="icon icon-normal-bed"></i>
    <?php echo $data['StandardFields']['BathsTotal']; ?>
  </div>
</div>