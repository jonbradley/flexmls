
<div class="property-filter widget">
    <div class="content">
    <?php echo $this->Form->create('Listing', array('url' => $url, 'type' => 'get')); ?>

      <div class="type control-group">
        <?php
          echo $this->Form->label('Property Type', null, array('class' => 'control-group'));
          $selected_property_type = (isset($this->request->query['PropertyType'])) ? $this->request->query['PropertyType'] : '';
          echo $this->Form->input('PropertyType', array(
            'label' => false,
            'div' => 'controls',
            'type' => 'select',
            'empty' => '-',
            'options' => array(
              "A" => "Residential", "B" => "MultiFamily", "C" => "Land", "D" => "Commercial", "E" => "Residential Rental", ),
            'selected' => $selected_property_type
          ));
        ?>
      </div>

      <div class="location control-group">
        <?php
          echo $this->Form->label('City', null, array('class' => 'control-group'));
          $selected_city = (isset($this->request->query['City'])) ? $this->request->query['City'] : '';
          echo $this->Form->input('City', array(
            'label' => false,
            'div' => 'controls',
            'type' => 'select',
            'empty' => '-',
            'options' => $filter['cities'],
            'selected' => $selected_city
          ));
        ?>
      </div>

      <div class="location control-group">
        <?php
          echo $this->Form->label('Postal Code', null, array('class' => 'control-group'));
          $selected_PostalCode = (isset($this->request->query['PostalCode'])) ? $this->request->query['PostalCode'] : '';
          echo $this->Form->input('PostalCode', array(
            'label' => false,
            'div' => 'controls',
            'type' => 'select',
            'empty' => '-',
            'options' => $filter['zipcodes'],
            'selected' => $selected_PostalCode
          ));
        ?>
      </div>

      <div class="location control-group">
        <?php
          echo $this->Form->label('County or Parish', null, array('class' => 'control-group'));
          $selected_CountyOrParish = (isset($this->request->query['CountyOrParish'])) ? $this->request->query['CountyOrParish'] : '';
          echo $this->Form->input('CountyOrParish', array(
            'label' => false,
            'div' => 'controls',
            'type' => 'select',
            'empty' => '-',
            'options' => $filter['counties'],
            'selected' => $selected_CountyOrParish
          ));
        ?>
      </div>

      <div class="price-from control-group">
        <?php
          echo $this->Form->label('Price From', null, array('class' => 'control-group'));
          $selected_min_price = (isset($this->request->query['min_price'])) ? $this->request->query['min_price'] : '';
          echo $this->Form->input('min_price', array(
            'label' => false,
            'div' => 'controls',
            'type' => 'text',
            'value' => $selected_min_price
          ));
        ?>
      </div>

      <div class="price-to control-group">
        <?php
          echo $this->Form->label('Price To', null, array('class' => 'control-group'));
          $selected_max_price = (isset($this->request->query['max_price'])) ? $this->request->query['max_price'] : '';
          echo $this->Form->input('max_price', array(
            'label' => false,
            'div' => 'controls',
            'type' => 'text',
            'value' => $selected_max_price
          ));
        ?>
      </div>

      <div class="area-to control-group">
        <?php
          echo $this->Form->label('Number of Bedrooms', null, array('class' => 'control-group'));
          $selected_num_bedrooms = (isset($this->request->query['BedsTotal'])) ? $this->request->query['BedsTotal'] : '';
          echo $this->Form->input('BedsTotal', array(
            'label' => false,
          'div' => 'controls',
            'type' => 'select',
            'empty' => '-',
            'options' => array(
              '1' => '1+', '2' => '2+', '3' => '3+', '4' => '4+', '5' => '5+', '6' => '6+'),
            'selected' => $selected_num_bedrooms
          ));
        ?>
      </div>

      <div class="area-to control-group">
        <?php
          echo $this->Form->label('Number of Bathrooms', null, array('class' => 'control-group'));
          $selected_num_full_bathrooms = (isset($this->request->query['BathsTotal'])) ? $this->request->query['BathsTotal'] : '';
          echo $this->Form->input('BathsTotal', array(
            'label' => false,
            'div' => 'controls',
            'type' => 'select',
            'empty' => '-',
            'options' => array(
              '1' => '1+', '2' => '2+', '3' => '3+', '4' => '4+', '5' => '5+', '6' => '6+'),
            'selected' => $selected_num_full_bathrooms
          ));
        ?>
      </div>

      <div class="form-actions">
        <?php echo $this->Form->submit('Search', array('class' => 'btn btn-primary btn-large', 'div' => false)); ?>
      </div>

      <?php echo $this->Form->end(); ?>
    </div>
</div>