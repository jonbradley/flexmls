</div>
<div id="footer-wrapper">

<div id="footer-top">
<div id="footer-top-inner" class="container">
<div class="row">

  <div class="span3">
    <div id="mostrecentproperties_widget-3" class="widget properties">
      <h2>Newest Listings</h2>
      <div class="content">
        <?php 
          if (!empty($recent_listings)) {
            foreach ($recent_listings as $key => $value) {
              echo $this->element('footer_listings', array('data' => $value)); 
            }
          }
        ?>
      </div>
    </div>
  </div>

  <div class="span3">
    <div id="nav_menu-2" class="widget our-blog">
      <h2>Recent News</h2>
      <div class="content">
        <?php 
        if (!empty($recent_posts)) {
          foreach ($recent_posts as $key => $value) {
            echo $this->element('footer_posts', array('data' => $value)); 
          }
        }
        ?>
      </div>
    </div>
  </div>

  <div class="span3">
    <div id="agents_widget-2" class="widget our-agents">
      <h2>Agents</h2>
      <div class="content">
        <?php 
        if (!empty($featured_agents)) {
          foreach ($featured_agents as $key => $value) {
            echo $this->element('featured_agents', array('data' => $value)); 
          }
        }
        ?>
      </div>
    </div>
  </div>

  <div class="span3">
    <div id="featuredproperties_widget-2" class="widget properties">
      <h2>Featured Properties</h2>
      <div class="content">
        <?php 
        if (!empty($featured_listings)) {
          foreach ($featured_listings as $key => $value) {
            echo $this->element('footer_listings', array('data' => $value)); 
          }
        }
        ?>
      </div>
    </div>
  </div>

  </div>
<!-- /.row -->
</div>
<!-- /#footer-top-inner -->
</div>
<!-- /#footer-top -->

<div class="footer-bottom">
  <div id="footer" class="footer container">
  <div id="footer-inner">
    <div class="row">
    <div class="span6">
      <div id="text-3" class="widget widget-text" >
      <div class="textwidget designed-by"  style="font-size:9pt">
        &copy; <?php echo date('Y'); ?> Home Run Real Estate, Inc. All Rights reserved <br />
        Website Designed by <a href="http://www.thefloridadesigngroup.com" target="new">Fort Lauderdale Website Design</a>
      </div>
      </div>
    </div>
    <!-- /.copyright -->

    <div class="span6">
      <div id="nav_menu-3" class="widget widget-nav_menu">
      <div class="menu-footer-links-container designed-by" style="font-size:9pt; color: orange">
        <a href="/privacy-policy">Privacy Policy</a> &nbsp; | &nbsp; <a href="/terms-of-use">Terms of Use</a>
      </div>
      </div>
    </div>
    <!-- /.span6 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /#footer-inner -->
  </div>
  <!-- /#footer -->
</div>
</div>
<!-- /#footer-wrapper -->
<script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?v=3&#038;sensor=true&#038;ver=3.6'></script>
<script type='text/javascript' src='/js/aviators-map.js'></script>
<script type='text/javascript' src='/js/gmap3.infobox.min.js'></script>
<script type='text/javascript' src='/js/bootstrap.min.js'></script>
<script type='text/javascript' src='/js/retina.js'></script>
<script type='text/javascript' src='/js/gmap3.clusterer.js'></script>
<script type='text/javascript' src='/js/jquery.ezmark.js'></script>
<script type='text/javascript' src='/js/carousel.js'></script>
<script type='text/javascript' src='/js/jquery.bxslider.js'></script>
<script type='text/javascript' src='/js/properta.js'></script>
<script type='text/javascript' src='/js/jquery.bxslider.min.js'></script>

    <script type="text/javascript">
      $(document).ready(function () {
        $('.thumb-detail').click(function () {
          var id = this.id;
          var src = $('#' + id + ' img').attr('src');
          $('.main').attr('src', src);
        });
      });
    </script>
</body>
</html>