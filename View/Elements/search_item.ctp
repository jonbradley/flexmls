
<div class="span4 listing">
  <?php 
  if (!empty($value['StandardFields']['Photos'])) {
    echo $this->Link->listing($this->Html->image($value['StandardFields']['Photos'][0]['Uri300'], array('style' => 'height: 200px; width: 300px;',)), $value);
  }
  else {
    echo $this->Link->listing($this->Html->image('thumb.png', array('style' => 'height: 200px; width: 300px;')), $value);
  }
  ?>
  <strong>
  <div class="price"><?php echo $this->Link->listing($this->Number->currency($value['StandardFields']['ListPrice'], 'USD'), $value); ?></div>
    <?php
    $address  = '';
    if (!empty($value['StandardFields']['StreetNumber']) && $value['StandardFields']['StreetNumber'] != '********') $address .= $value['StandardFields']['StreetNumber'] .' ';
    if (!empty($value['StandardFields']['StreetName'])   && $value['StandardFields']['StreetName'] != '********')   $address .= $value['StandardFields']['StreetName']   .' ';
    if (!empty($value['StandardFields']['StreetSuffix']) && $value['StandardFields']['StreetSuffix'] != '********') $address .= $value['StandardFields']['StreetSuffix'];
    ?>
    <?php echo $this->Link->listing(ucwords(strtolower($address)), $value) ?>  <br />
  </strong>
  <span class="small">
    
  <?php echo $value['StandardFields']['BedsTotal']; ?>/
  <?php 
  echo $value['StandardFields']['BathsFull']; 
  if (!empty($value['StandardFields']['BathsHalf'])) echo '/'. $value['StandardFields']['BathsHalf'];
  ?> | 
  <?php echo $value['StandardFields']['BuildingAreaTotal']; ?> Sq. Ft. | 
  <?php echo $value['StandardFields']['PropertySubType']; ?>
  </span>
  <br />
  <span class="small"><em><?php echo ucwords(strtolower($value['StandardFields']['SubdivisionName'])); ?></em></span>
</div>