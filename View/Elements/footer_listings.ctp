<?php

$address = $this->Link->address($data);

// set the image
$image = $this->Html->image($data['StandardFields']['Photos'][0]['Uri300'], array('class' => 'thumbnail-image'));
?>

<div class="property clearfix">
  <div class="image">
    <?php echo $this->Link->listing($image, $data); ?>
  </div>
  <div class="wrapper">
    <div class="title">
      <h3><?php echo $this->link->listing($address, $data); ?></h3>
    </div>
    <div class="location"><?php echo $data['StandardFields']['City']; ?></div>
    <div class="price"> <?php echo $this->Number->currency($data['StandardFields']['ListPrice'] ,'USD'); ?></div>
  </div>
</div>