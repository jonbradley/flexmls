
<!-- START REVOLUTION SLIDER 3.0.5 fullwidth mode -->


<div id="frontpage-slider-wrapper" class="revolution-slider fullwidthbanner-container"
     style="margin:0px auto;color:#333;background-color:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:450px;">
    <div id="frontpage-slider" class="rev_slider fullwidthabanner" style="display:none;max-height:450px;height:450px;color:#333;">
        <ul>
            <li data-transition="fade" data-slotamount="7" data-masterspeed="300">
                <img src="/img/revolution/bg1.jpg" alt="bg1"
                     data-fullwidthcentering="on">

                <div class="tp-caption very_large_text sfr"
                     data-x="10"
                     data-y="77" data-speed="300"
                     data-start="500"
                     data-easing="easeInOutQuad">Home Run Real Estate
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="10"
                     data-y="155" data-speed="300"
                     data-start="600"
                     data-easing="easeInOutQuad">offers a friendly,
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="9"
                     data-y="207" data-speed="300"
                     data-start="700"
                     data-easing="easeInOutQuad">easy way for you to select
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="10"
                     data-y="262" data-speed="300"
                     data-start="800"
                     data-easing="easeInOutQuad"> the best area to move to in Florida!
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="10"
                     data-y="317" data-speed="300"
                     data-start="1000"
                     data-easing="easeInOutQuad"> Look around now and find the home of your dreams!
                </div>

            </li>
            <li data-transition="fade" data-slotamount="7" data-masterspeed="300">
                <img src="/img/revolution/bg2.jpg" alt="bg2"
                     data-fullwidthcentering="on">
                     
                <div class="tp-caption very_large_text sfr"
                     data-x="10"
                     data-y="77" data-speed="300"
                     data-start="500"
                     data-easing="easeInOutQuad">At Home Run Real Estate,
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="10"
                     data-y="155" data-speed="300"
                     data-start="600"
                     data-easing="easeInOutQuad">we have over 250
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="9"
                     data-y="207" data-speed="300"
                     data-start="700"
                     data-easing="easeInOutQuad">real estate associates
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="10"
                     data-y="262" data-speed="300"
                     data-start="800"
                     data-easing="easeInOutQuad"> with the experience
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="10"
                     data-y="317" data-speed="300"
                     data-start="1000"
                     data-easing="easeInOutQuad">and professionalism that you need.
                </div>

            </li>
            <li data-transition="fade" data-slotamount="7" data-masterspeed="300">
                <img src="/img/revolution/bg3.jpg" alt="bg3"
                     data-fullwidthcentering="on">
                <div class="tp-caption very_large_text sfr"
                     data-x="10"
                     data-y="77" data-speed="300"
                     data-start="500"
                     data-easing="easeInOutQuad">At Home Run Real Estate,
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="10"
                     data-y="155" data-speed="300"
                     data-start="600"
                     data-easing="easeInOutQuad">we believe choosing a Realtor
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="9"
                     data-y="207" data-speed="300"
                     data-start="700"
                     data-easing="easeInOutQuad"> is the same as picking a business partner.
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="10"
                     data-y="262" data-speed="300"
                     data-start="800"
                     data-easing="easeInOutQuad"> We partner with you 
                </div>

                <div class="tp-caption large_text sfr"
                     data-x="10"
                     data-y="317" data-speed="300"
                     data-start="1000"
                     data-easing="easeInOutQuad"> in finding your dream home.
                </div>

            </li>
        </ul>
    </div>
</div>

<script type="text/javascript">

    jQuery('#frontpage-slider').show().revolution(
      {
          delay: 9000,
          startwidth: 960,
          startheight: 450,
          hideThumbs: 200,

          thumbWidth: 100,
          thumbHeight: 50,
          thumbAmount: 3,

          navigationType: "bullet",
          navigationArrows: "solo",
          navigationStyle: "round",

          touchenabled: "on",
          onHoverStop: "off",

          navigationHAlign: "center",
          navigationVAlign: "bottom",
          navigationHOffset: 0,
          navigationVOffset: 20,

          soloArrowLeftHalign: "left",
          soloArrowLeftValign: "center",
          soloArrowLeftHOffset: 20,
          soloArrowLeftVOffset: 0,

          soloArrowRightHalign: "right",
          soloArrowRightValign: "center",
          soloArrowRightHOffset: 20,
          soloArrowRightVOffset: 0,

          shadow: 0,
          fullWidth: "on",
          fullScreen: "off",

          stopLoop: "on",
          stopAfterLoops: -1,
          stopAtSlide: -1,

          shuffle: "off",

          hideSliderAtLimit: 0,
          hideCaptionAtLimit: 0,
          hideAllCaptionAtLilmit: 0,
          startWithSlide: 0,
          fullScreenOffsetContainer: ""
      });
</script>

<!-- END REVOLUTION SLIDER -->