
    <div class="blog clearfix">
      <div class="postdate">
        <div class="postmonth"><?php echo $this->Time->format('M', $data['Post']['created']) ?></div>
        <div class="postday"><?php echo $this->Time->format('d', $data['Post']['created']) ?></div>
      </div>


      <div class="name">
      <?php 
      echo $this->Html->link(
        $data['Post']['title'],
        '/blog/'. $data['Post']['slug'] .'.html',
        array('escape' => false)
      ); 
      ?>
      </div>

      <div class="post">
        <?php echo $this->Text->truncate($data['Post']['body'], 75, array('exact' => false, 'ellipsis' => ' ..')); ?> <br />
        <?php 
        echo $this->Html->link(
          'Read More &raquo;',
          '/blog/'. $data['Post']['slug'] .'.html',
          array('escape' => false)
        ); 
        ?>
      </div>
    </div>
