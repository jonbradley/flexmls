<!DOCTYPE html>



<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Pragma" content="no-cache"/ >

  <link rel="shortcut icon" href="/img/favicon.png" type="image/png" />

  <!--[if lt IE 9]>
  <script src="/js/html5.js" type="text/javascript"></script>
  <![endif]-->

  <?php
  echo $this->Html->meta(array('keywords' => $page_name['keywords']));
  echo $this->Html->meta(array('description' => $page_name['description']));
  ?>

  <meta name='robots' content='noindex,nofollow'/>

  <link rel='stylesheet' id='font-css'
      href='http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C700%2C300&#038;subset=latin%2Clatin-ext&#038;ver=3.6'
      type='text/css' media='all'/>
   <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>


  <link rel='stylesheet' id='revolution-fullwidth' href='/libraries/rs-plugin/css/fullwidth.css' type='text/css' media='all'/>
  <link rel='stylesheet' id='revolution-settings' href='/libraries/rs-plugin/css/settings.css' type='text/css' media='all'/>
  <link rel='stylesheet' id='bootstrap-css' href='/libraries/bootstrap/css/bootstrap.min.css' type='text/css' media='all'/>
  <link rel='stylesheet' id='bootstrap-responsive-css' href='/libraries/bootstrap/css/bootstrap-responsive.min.css' type='text/css' media='all'/>

  <link rel='stylesheet' id='pictopro-normal-css' href='/icons/pictopro-normal/style.css' type='text/css' media='all'/>
  <link rel='stylesheet' id='justvector-web-font-css' href='/icons/justvector-web-font/stylesheet.css' type='text/css' media='all'/>
  <link rel='stylesheet' id='chosen-css' href='/libraries/chosen/chosen.css' type='text/css' media='all'/>

  <link rel='stylesheet' id='aviators-css' href='/css/jquery.bxslider.css' type='text/css' media='all'/>
  <link rel='stylesheet' id='properta-css' href='/css/application.css' type='text/css' media='all'/>

  <script type='text/javascript' src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
  <script type='text/javascript' src='/js/aviators-settings.js'></script>
  <script type='text/javascript' src='/libraries/chosen/chosen.jquery.min.js'></script>
  <script type='text/javascript' src='/libraries/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>
  <script type='text/javascript' src='/libraries/rs-plugin/js/jquery.themepunch.plugins.min.js'></script>

  <title> <?php echo $page_name['title']; ?> </title>

    <script type="text/javascript">//<![CDATA[
      // Google Analytics for WordPress by Yoast v4.3.3 | http://yoast.com/wordpress/google-analytics/
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-5182948-51']);
              _gaq.push(['_trackPageview']);
      (function () {
          var ga = document.createElement('script');
          ga.type = 'text/javascript';
          ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(ga, s);
      })();
      //]]></script>

</head>

<body class="home page page-template">

<div class="top">
  <div id="header">
    <div class="container">
      <div class="top-inner inverted">
        <div class="header clearfix">
          <div class="branding pull-left">
            <div class="logo">
              <a href="/" title="Home">
                <img src="/img/logo.png" alt="Home Run Real Estate, Inc." />
              </a>
            </div>
            </div>
      
        
          <div class="contact-top">
            <ul class="menu nav">
              <li style="font-size:14pt; padding-top: 4px">Follow us On:</li>
              <li>&nbsp;</li>
              <li><a href="http://www.facebook.com/HomeRunRealEstateFlorida" target="new" rel="external"><img src="/icons/social-fb.png" alt="Facebook" /></a></li>
              <li><a href="https://twitter.com/homerunre" target="new" rel="external"><img src="/icons/social-twitter.png" alt="twitter" /></a></li>
              <li><a href="http://www.linkedin.com/groupInvitation?groupID=2422158" target="new" rel="external"><img src="/icons/social-linkedin.png" alt="linkedin" /></a></li>
              <li><a href="http://www.youtube.com/HomeRunRealEstate1" target="new" rel="external"><img src="/icons/social-youtube.png" alt="youtube" /></a></li>
            </ul>
            Toll Free: <a href="tel://800-967-7074">(800) 967-7074</a> or Phone: <a href="tel://561-433-3836">(561) 433-3836</a> <br />
            <a href="/main-offices">Click here to find your local office throughout the State of Florida</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--

  <div class="why-us">
    <div class="container">
    HOME RUN REAL ESTATE IS LEADING THE WAY IN FLORIDA…<span class="blue">CLICK HERE TO LEARN MORE</span></div>
  </div>
  -->

<div class="mainmenu">
  <div class="container">
    <div class="top-inner inverted">
      <!-- /.header -->
      <div class="navigation navbar clearfix">
      <div class="pull-left">
        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <div class="nav-collapse collapse">
          <ul id="menu-main" class="nav">

            <li class="menu-item">
              <a href="/">Home</a>
            </li>

            <li class="menu-item menu-item-parent">
              <a href="#">Search for a Home</a>

              <ul class="sub-menu">
                <li class="menu-item"><a href="/search">Search All Listings</a></li>
                <li class="menu-item"><a href="/search_ours">Search Our Listings</a></li>
                <li class="menu-item"><a href="/most_recent">All Recent Listings</a></li>
                <li class="menu-item"><a href="/our_most_recent">Our Recent Listings</a></li>
                <li class="menu-item"><a href="/frequently-asked-questions">Frequently Asked Questions</a></li>
                <li class="menu-item"><a href="/florida-links">Florida Links</a></li>
              </ul>
            </li>

            <li class="menu-item menu-item-parent">
              <a href="/sell-a-home">Sell A Home</a>

              <ul class="sub-menu">
                <li class="menu-item"><a href="/the-selling-process">The Selling Process</a></li>
                <li class="menu-item"><a href="/selling-a-home-frequently-asked-questions">Frequently Asked Questions</a></li>
                <li class="menu-item"><a href="/marketing-plan">Marketing Plan</a></li>
                <li class="menu-item"><a href="/florida-links">Florida Links</a></li>
              </ul>
            </li>

            <li class="menu-item">
              <a href="/agents">Agent Directory</a>
            </li>


            <li class="menu-item menu-item-parent">
              <a href="/the-home-run-difference">The Home Run Difference</a>

              <ul class="sub-menu">
                <li class="menu-item"><a href="/about-home-run">About Home Run</a></li>
                <li class="menu-item"><a href="/about-the-broker">About The Broker</a></li>
                <li class="menu-item"><a href="/why-choose-home-run">Why Choose Home Run</a></li>
                <li class="menu-item"><a href="/marketing-plan">Marketing Plan</a></li>
              </ul>
            </li>

            <li class="menu-item">
              <a href="http://shaneturner.supremelending.com/" target="new" rel="external">Apply For A Loan</a>
            </li>

            <li class="menu-item">
              <a href="/blog">Our Blog</a>
            </li>

            <li class="menu-item">
              <a href="/careers">Careers</a>
            </li>

            <li class="menu-item menu-item-parent">
              <a href="/contact-us">Contact</a>

              <ul class="sub-menu">
                <li class="menu-item"><a href="/main-offices">Office Locations</a></li>
              </ul>
            </li>

          </ul>
        </div>
      </div>
      <!-- /.pull-right -->

    </div>

      <!--
      <div class="breadcrumb pull-left">
        <a title="Go to Home Run Real Estate." href="/" class="home">Home Run Real Estate</a> &gt; Page
      </div>
      -->
    </div>
  </div>
</div>
</div>
<div id="content" class="clearfix">