<div class="sidebar span3">
  <h2>Search Properties</h2>
  <?php  echo $this->element('search', array('url' => $url),
                array('cache' => array('config' => 'longterm', 'key' => 'search'))
              ); ?>
         
  <div id="mostrecentproperties_widget-2" class="widget properties">
    <h2>Newest Listings</h2>
    <div class="content">
      <?php
      if (!empty($recent_listings)) {
        foreach ($recent_listings as $key => $value) {
          echo $this->element('sidebar_recent_listings', array('data' => $value),
                array('cache' => array('config' => 'longterm', 'key' => 'sidebar_recent-'. $value['StandardFields']['ListingId']))
              );
        }
      }
      ?>
    </div>
  </div>                   
</div>