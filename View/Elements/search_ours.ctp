<?php echo $this->Form->create('Listing', array('url' => '/search', 'type' => 'get')); ?>
<div class="row-fluid">

  <div class="span4">
    <?php 

    $selected_id = (isset($this->request->query['MlsId'])) ? $this->request->query['MlsId'] : '';
    echo $this->Form->input('MlsId', array(
      'label' => false,
      'div' => false,
      'type' => 'text',
      'placeholder' => 'MLS Number',
      'value' => $selected_id
    ));

    $selected_property_type = (isset($this->request->query['PropertyType'])) ? $this->request->query['PropertyType'] : '';
    echo $this->Form->input('PropertyType', array(
      'label' => false,
      'div' => false,
      'type' => 'select',
      'empty' => 'Property Type',
      'options' => array(
        "A" => "Residential", "B" => "MultiFamily", "C" => "Land", "D" => "Commercial", "E" => "Residential Rental", ),
      'selected' => $selected_property_type
    ));

    $selected_price = (isset($this->request->query['price'])) ? $this->request->query['price'] : '';
    echo $this->Form->input('price', array(
      'label' => false,
      'div' => false,
      'type' => 'select',
      'empty' => 'Please select a Price Range',
      'options' => array(
        '500000' => '$500,000+', '550000' => '$550,000+', '650000' => '$650,000+', '750000' => '$750,000+', '850000' => '$850,000+', '1000000' => '$1,000,000+', ),
      'selected' => $selected_price
    ));
    ?>
  </div>
  <div class="span4">
    <?php 

  $selected_CountyOrParish = (isset($this->request->query['CountyOrParish'])) ? $this->request->query['CountyOrParish'] : '';
  echo $this->Form->input('CountyOrParish', array(
    'label' => false,
    'div' => false,
    'type' => 'select',
    'empty' => 'Select a County',
    'options' => array(
      'Alachua' => 'Alachua', 'Baker' => 'Baker', 'Bay' => 'Bay', 'Bradford' => 'Bradford', 'Brevard' => 'Brevard', 'Broward' => 'Broward', 'Calhoun' => 'Calhoun', 'Charlotte' => 'Charlotte', 'Citrus' => 'Citrus', 'Clay' => 'Clay', 'Collier' => 'Collier', 'Columbia' => 'Columbia', 'DeSoto' => 'DeSoto', 'Dixie' => 'Dixie', 'Duval' => 'Duval', 'Escambia' => 'Escambia', 'Flagler' => 'Flagler', 'Franklin' => 'Franklin', 'Gadsden' => 'Gadsden', 'Gilchrist' => 'Gilchrist', 'Glades' => 'Glades', 'Gulf' => 'Gulf', 'Hamilton' => 'Hamilton', 'Hardee' => 'Hardee', 'Hendry' => 'Hendry', 'Hernando' => 'Hernando', 'Highlands' => 'Highlands', 'Hillsborough' => 'Hillsborough', 'Holmes' => 'Holmes', 'Indian River' => 'Indian River', 'Jackson' => 'Jackson', 'Jefferson' => 'Jefferson', 'Lafayette' => 'Lafayette', 'Lake' => 'Lake', 'Lee' => 'Lee', 'Leon' => 'Leon', 'Levy' => 'Levy', 'Liberty' => 'Liberty', 'Macon' => 'Macon', 'Madison' => 'Madison', 'Manatee' => 'Manatee', 'Marion' => 'Marion', 'Martin' => 'Martin', 'Miami-Dade' => 'Miami-Dade', 'Monroe' => 'Monroe', 'Nassau' => 'Nassau', 'Okaloosa' => 'Okaloosa', 'Okeechobee' => 'Okeechobee', 'Orange' => 'Orange', 'Osceola' => 'Osceola', 'Out of Country' => 'Out of Country', 'Out of State' => 'Out of State', 'Palm Beach' => 'Palm Beach', 'Pasco' => 'Pasco', 'Pinellas' => 'Pinellas', 'Polk' => 'Polk', 'Putnam' => 'Putnam', 'Rhone-Alpes' => 'Rhone-Alpes', 'Roane' => 'Roane', 'Santa Rosa' => 'Santa Rosa', 'Sarasota' => 'Sarasota', 'Select One' => 'Select One', 'Seminole' => 'Seminole', 'St. Johns' => 'St. Johns', 'St. Lucie' => 'St. Lucie', 'Sumter' => 'Sumter', 'Suwannee' => 'Suwannee', 'Taylor' => 'Taylor', 'Union' => 'Union', 'Volusia' => 'Volusia', 'Wakulla' => 'Wakulla', 'Walton' => 'Walton', 'Washington' => 'Washington'),
    'selected' => $selected_CountyOrParish
  ));

  $selected_city_name = (isset($this->request->query['City'])) ? $this->request->query['City'] : '';
  echo $this->Form->input('City', array(
    'label' => false,
    'div' => false,
    'type' => 'text',
    'placeholder' => 'Enter a City',
    'value' => $selected_city_name
  ));

  $selected_PostalCode = (isset($this->request->query['PostalCode'])) ? $this->request->query['PostalCode'] : '';
  echo $this->Form->input('PostalCode', array(
    'label' => false,
    'div' => false,
    'type' => 'text',
    'placeholder' => 'Enter a Postal Code',
    'value' => $selected_PostalCode
  ));
    ?>
  </div>
  <div class="span4">
    <?php 

    $selected_num_bedrooms = (isset($this->request->query['BedsTotal'])) ? $this->request->query['BedsTotal'] : '';
    echo $this->Form->input('BedsTotal', array(
      'label' => false,
      'div' => false,
      'type' => 'select',
      'empty' => 'Number of Bedrooms',
      'options' => array(
        '1' => '1+', '2' => '2+', '3' => '3+', '4' => '4+', '5' => '5+', '6' => '6+'),
      'selected' => $selected_num_bedrooms
    ));

    $selected_num_full_bathrooms = (isset($this->request->query['BathsTotal'])) ? $this->request->query['BathsTotal'] : '';
    echo $this->Form->input('BathsTotal', array(
      'label' => false,
      'div' => false,
      'type' => 'select',
      'empty' => 'Number of Bathrooms',
      'options' => array(
        '1' => '1+', '2' => '2+', '3' => '3+', '4' => '4+', '5' => '5+', '6' => '6+'),
      'selected' => $selected_num_full_bathrooms
    ));
    
    echo $this->Form->submit('Search', array('class' => 'btn btn-info', 'div' => false));
    ?>
    <a href="/search" class="btn btn-danger pull-right">Clear Results</a>
  </div>
  
</div>
<?php 







echo $this->Form->end();
?>