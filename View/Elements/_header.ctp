<!DOCTYPE html>



<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Pragma" content="no-cache"/ >

    <link rel="shortcut icon" href="/img/favicon.png" type="image/png" />

    <!--[if lt IE 9]>
    <script src="/js/html5.js" type="text/javascript"></script>
    <![endif]-->

    <?php
    echo $this->Html->meta(array('keywords' => $page_name['keywords']));
    echo $this->Html->meta(array('description' => $page_name['description']));
    ?>

    <meta name='robots' content='noindex,nofollow'/>

    <link rel='stylesheet' id='font-css'
          href='http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C700%2C300&#038;subset=latin%2Clatin-ext&#038;ver=3.6'
          type='text/css' media='all'/>
     <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>


    <link rel='stylesheet' id='revolution-fullwidth' href='/libraries/rs-plugin/css/fullwidth.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='revolution-settings' href='/libraries/rs-plugin/css/settings.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='bootstrap-css' href='/libraries/bootstrap/css/bootstrap.min.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='bootstrap-responsive-css' href='/libraries/bootstrap/css/bootstrap-responsive.min.css' type='text/css' media='all'/>

    <link rel='stylesheet' id='pictopro-normal-css' href='/icons/pictopro-normal/style.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='justvector-web-font-css' href='/icons/justvector-web-font/stylesheet.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='chosen-css' href='/libraries/chosen/chosen.css' type='text/css' media='all'/>

    <link rel='stylesheet' id='aviators-css' href='/css/jquery.bxslider.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='properta-css' href='/css/application.css' type='text/css' media='all'/>

    <script type='text/javascript' src='http://code.jquery.com/jquery-1.7.2.min.js'></script>
    <script type='text/javascript' src='/js/aviators-settings.js'></script>
    <script type='text/javascript' src='/libraries/chosen/chosen.jquery.min.js'></script>
    <script type='text/javascript' src='/libraries/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>
    <script type='text/javascript' src='/libraries/rs-plugin/js/jquery.themepunch.plugins.min.js'></script>

    <title>Home Run Real Estate, Inc.</title>
</head>

<body class="home page page-template">

<div class="top">
    <div id="header">
        <div class="container">
            <div class="top-inner inverted">
                <div class="header clearfix">
                    <div class="branding pull-left">
                        <div class="logo">
                            <a href="/" title="Home">
                                <img src="/img/logo.png" alt="Home Run Real Estate, Inc." />
                            </a>
                        </div>
                        </div>
            
                
                    <div class="contact-top">
                        <ul class="menu nav">
                            <li style="font-size:14pt; padding-top: 4px">Follow us On:</li>
                            <li>&nbsp;</li>
                            <li><a href="http://www.facebook.com/HomeRunRealEstateFlorida" target="new" rel="external"><img src="/icons/social-fb.png" alt="Facebook" /></a></li>
                            <li><a href="https://twitter.com/homerunre" target="new" rel="external"><img src="/icons/social-twitter.png" alt="twitter" /></a></li>
                            <li><a href="http://www.linkedin.com/groupInvitation?groupID=2422158" target="new" rel="external"><img src="/icons/social-linkedin.png" alt="linkedin" /></a></li>
                            <li><a href="http://www.youtube.com/HomeRunRealEstate1" target="new" rel="external"><img src="/icons/social-youtube.png" alt="youtube" /></a></li>
                        </ul>
                        Toll Free: <a href="tel://800-967-7074">(800) 967-7074</a> or Phone: <a href="tel://561-433-3836">(561) 433-3836</a> <br />
                        3898 Via Poinciana, Suite #12, Lake Worth, FL 33467 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="why-us">
        <div class="container">
        HOME RUN REAL ESTATE IS LEADING THE WAY IN FLORIDA…<span class="blue">CLICK HERE TO LEARN MORE</span></div>
    </div>

<div class="mainmenu">
    <div class="container">
        <div class="top-inner inverted">
            <!-- /.header -->
            <div class="navigation navbar clearfix">
            <div class="pull-left">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <div class="nav-collapse collapse">
                    <ul id="menu-main" class="nav">
                        <li class="menu-item active-menu-item menu-item-parent">
                            <a href="">Homepage</a>
                            <ul class="sub-menu">
                                <li class="menu-item"><a href="frontpage-slider.html">Revolution slider</a></li>
                                <li class="menu-item"><a href="frontpage-map-vertical.html">Hompage - Vertical filter</a></li>
                                <li class="menu-item"><a href="index.html">Hompage - Horizontal filter</a></li>
                            </ul>
                        </li>

                        <li class="menu-item menu-item-parent">
                            <a href="#">Listings</a>

                            <ul class="sub-menu">
                                <li class="menu-item"><a href="properties.html">Properties listing</a></li>
                                <li class="menu-item"><a href="property-detail.html">Property detail</a></li>

                                <li class="menu-item"><a href="agencies.html">Agencies listing</a></li>
                                <li class="menu-item"><a href="agency-detail.html">Agencies detail</a></li>

                                <li class="menu-item"><a href="agents.html">Agents listing</a></li>
                                <li class="menu-item"><a href="agent-detail.html">Agent detail</a></li>

                                <li class="menu-item"><a href="register.html">Register</a></li>
                                <li class="menu-item"><a href="login.html">Login</a></li>
                            </ul>
                        </li>

                        <li class="menu-item menu-item-parent">
                            <a href="#">Pages</a>
                            <ul class="sub-menu">
                                <li class="menu-item"><a href="404.html">404 page</a></li>
                                <li class="menu-item"><a href="faq.html">FAQ page</a></li>
                                <li class="menu-item"><a href="pricing.html">Pricing</a></li>
                            </ul>
                        </li>

                        <li class="menu-item menu-item-parent">
                            <a href="#">Submissions</a>
                            <ul class="sub-menu">
                                <li class="menu-item"><a href="submissions.html">List submissions</a></li>
                                <li class="menu-item"><a href="add.html">Add submission</a></li>
                                <li class="menu-item"><a href="edit.html">Edit submission</a></li>
                            </ul>
                        </li>

                        <li class="menu-item menu-item-parent">
                            <a href="#">Templates</a>
                            <ul class="sub-menu">
                                <li class="menu-item"><a href="default-left.html">Left sidebar</a></li>
                                <li class="menu-item"><a href="default-right.html">Right sidebar</a></li>
                                <li class="menu-item"><a href="default-full.html">Full width</a></li>
                            </ul>
                        </li>

                        <li class="menu-item">
                            <a href="contact.html">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.pull-right -->

        </div>

            <!--
            <div class="breadcrumb pull-left">
                <a title="Go to Home Run Real Estate." href="/" class="home">Home Run Real Estate</a> &gt; Page
            </div>
            -->
        </div>
    </div>
</div>
</div>