
<div class="row">
  <div class="sidebox">
    <div class="header">Main Menu</div>
    <ul>
      <li><?php echo $this->Html->link('Users', array('controller' => 'users', 'action' => 'index', 'admin' => true)); ?></li>
      <li><?php echo $this->Html->link('Pages', array('controller' => 'pages', 'action' => 'index', 'admin' => true)); ?></li>
      <li><?php echo $this->Html->link('Posts', array('controller' => 'posts', 'action' => 'index', 'admin' => true)); ?></li>
      <li><?php echo $this->Html->link('Agents', array('controller' => 'agents', 'action' => 'index', 'admin' => true)); ?></li>
      <li><?php echo $this->Html->link('Featured Listings', array('controller' => 'featured_listings', 'action' => 'index', 'admin' => true)); ?></li>
      <li><?php echo $this->Html->link('SEO', array('controller' => 'seo', 'action' => 'index', 'admin' => true)); ?></li>
    </ul>
  </div>
</div>