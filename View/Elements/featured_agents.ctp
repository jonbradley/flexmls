
    <div class="agent clearfix">
      <div class="image">
        <?php 
        echo $this->Html->link(
          $this->Html->image('agents/'. $data['Agent']['photo'], array('alt' => $data['Agent']['name'], 'style' => 'height: 70px')),
          '/agents/'. $data['Agent']['slug'] .'.html',
          array('escape' => false)
        ); 
        ?>
      </div>

      <div class="name">
      <?php 
      echo $this->Html->link(
        $data['Agent']['name'],
        '/agents/'. $data['Agent']['slug'] .'.html',
        array('escape' => false)
      ); 
      ?>
      </div>

      <div class="phone">
        <i class="icon icon-normal-phone"></i>
        <?php echo $data['Agent']['phone']; ?>
      </div>

      <div class="email">
        <i class="icon icon-normal-mail"></i>
        <?php echo $this->Text->autoLinkEmails($data['Agent']['email']); ?>
      </div>
    </div>
