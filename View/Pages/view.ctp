<div class="container">
  <div class="row">
    <?php echo $this->element('sidebar', array('url' => '/search')); ?>
    <div id="main" class="span9">

      <article id="post-<?php echo $data['Page']['id']; ?>" class="clearfix post-<?php echo $data['Page']['id']; ?> page type-page status-publish entry ">

        <div class="row-fluid">
          <div class="span9"><h1 class="page-header"><?php echo $data['Page']['title']; ?></h1></div>
          <div class="span3"></div>
        </div>

        <div class="entry-content-main">
          <?php 
          $body = str_replace(array('”', '“'), "", $data['Page']['body']);
          echo $body; 
          ?>
        <?php 
        if ($data['Page']['contact_form'] == 1) { 
          $form_name =  strtolower(str_replace(' ', '-', $data['Page']['form_name']));
          echo $this->element('../Contacts/'. $form_name); 
        } 
        ?>
        </div>

      </article>

    </div>
  </div>
</div>