<h2>Edit Page</h2>
<div class="users form">
<?php echo $this->Form->create('Page'); ?>
    <fieldset>
        <?php 
        echo $this->Form->input('title', array('style' => 'width: 100%;'));
        echo $this->Form->input('body', array('style' => 'width: 100%; height: 500px'));
        echo $this->Form->input('slug', array('style' => 'width: 100%;'));
        echo '<hr>';
        echo $this->Form->input('seo_title', array('style' => 'width: 100%;'));
        echo $this->Form->input('seo_meta', array('style' => 'width: 100%;'));
        echo $this->Form->input('seo_description', array('style' => 'width: 100%;'));
        echo '<hr>';
        echo $this->Form->input('contact_form', array(
            'options' => array(
                0 => 'No', 1 => 'Yes'
            ),
            'div' => 'input select contact_form'
        ));
        echo $this->Form->input('form_name', array(
            'empty' => 'Select a Form', 
            'div' => 'input select form_name',
            'label' => 'Contact Form Name',
            'options' => array(
                'Contact Us' => 'Contact Us',
                'Sell With Us' => 'Sell With Us'
            )
        ));
        echo '<hr>';
    ?>
    </fieldset>
<?php 
echo $this->Form->submit('Submit', array('class' => 'btn')); 
echo $this->Form->end();
?>
</div>