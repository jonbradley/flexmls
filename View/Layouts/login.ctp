<?php echo $this->Html->docType('html5'); ?>
<html lang="en">
<head>
  <title>Home Run Real Estate Admin</title>
  <?php echo $this->Html->meta(array('name' => 'robots', 'content' => 'noindex')); ?>
  <?php echo $this->Html->css(array('/libraries/bootstrap/css/bootstrap.min', 'style', 'sidebar', 'admin')); ?>
</head>

<body>

  <div class="container">
    <div id="header">
      <div class="row">
        <div id="logo" class="span5">
          <a href="/"><img src="/img/logo.png" alt="The Diamond Group" /></a>
        </div>
      </div>
    </div>
    <div id="content">
      <?php $this->Session->flash(); ?>
      <?php echo $content_for_layout; ?>
    </div>
  </div>

</body>
</html>
