<?php echo $this->Html->docType('html5'); ?>
<html lang="en">
<head>
  <title>Home Run Real Estate Admin</title>
  <?php echo $this->Html->meta(array('name' => 'robots', 'content' => 'noindex')); ?>
  <?php echo $this->Html->css(array('/libraries/bootstrap/css/bootstrap.min', 'admin/styles', 'sidebar', 'admin')); ?>
</head>

<body>

  <div class="container">
    <div id="header">
      <div class="row">
        <div id="logo" class="span5">
          <a href="/"><img src="/img/logo.png" alt="The Diamond Group" /></a>
        </div>
      </div>
    </div>
    <div id="content">
      <div class="row">&nbsp;</div>
      <div class="row">
        <div class="span3">
          <?php 
          echo $this->element('admin/sidebar', array('cache' => 'long_view')); 
          ?>
        </div>
        <div class="span9">
          <?php $this->Session->flash(); ?>
          <?php echo $content_for_layout; ?>
        </div>
      </div>
    </div>
  </div>

  <?php echo $this->element('admin/scripts', array('cache' => 'long_view')); ?>
</body>
</html>
