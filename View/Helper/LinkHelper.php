<?php

class LinkHelper extends AppHelper
{
    public $helpers = array('Html');

    public function listing($text, $data)
    {
        return $this->Html->link($text, self::url($data), array('escape' => false));
    }

    public function generateLink($data)
    {
        $link  = '/'. self::clean($data['StandardFields']['City']);
        $link .= '/'. $data['StandardFields']['ListingId'] .'/';
        if (!empty($data['StandardFields']['StreetNumber']) &&
            $data['StandardFields']['StreetNumber'] != '********'
        ) {
            $link .= self::clean($data['StandardFields']['StreetNumber']) .'-';
        }
        if (!empty($data['StandardFields']['StreetName']) &&
            $data['StandardFields']['StreetName'] != '********') {
            $link .= self::clean($data['StandardFields']['StreetName']) .'-';
        }
        if (!empty($data['StandardFields']['StreetSuffix']) &&
            $data['StandardFields']['StreetSuffix'] != '********') {
            $link .= self::clean($data['StandardFields']['StreetSuffix']) .'-';
        }
        $link .= self::clean($data['StandardFields']['City'])  .'-';
        $link .= self::clean($data['StandardFields']['StateOrProvince']) .'-';
        $link .= self::clean($data['StandardFields']['PostalCode']) .'.html';
        return $link;
    }

    public function clean($text)
    {
        $text = str_replace(' # ', ' ', $text);
        $text = str_replace(' ', '-', $text);
        return $text;
    }

    public function address($data)
    {
                // set the address
        $address  = '';
        if (!empty($data['StandardFields']['StreetNumber']) && $data['StandardFields']['StreetNumber']  != '********') $address .= ucwords(strtolower($data['StandardFields']['StreetNumber'])) .' ';
        if (!empty($data['StandardFields']['StreetName'])   && $data['StandardFields']['StreetName']    != '********') $address .= ucwords(strtolower($data['StandardFields']['StreetName']))   .' ';
        if (!empty($data['StandardFields']['StreetSuffix']) && $data['StandardFields']['StreetSuffix']  != '********') $address .= ucwords(strtolower($data['StandardFields']['StreetSuffix']))  .' ';
        $address .= self::clean(ucwords(strtolower($data['StandardFields']['City'])))  .', ';
        $address .= self::clean(strtoupper($data['StandardFields']['StateOrProvince'])) .' ';
        $address .= self::clean(ucwords(strtolower($data['StandardFields']['PostalCode'])));

        $search = array(
            'Street', 'Drive', 'Road', 'Avenue', 'Court', 'Lane', 'Boulevard'
        );
        $replace = array(
            'St', 'Dr', 'Rd', 'Ave', 'Ct', 'Ln', 'Blvd'
        );

        return str_replace($search, $replace, $address);
    }

    public function address_short($data)
    {
                // set the address
        $address  = '';
        if (!empty($data['StandardFields']['StreetNumber']) &&
            $data['StandardFields']['StreetNumber']  != '********') {
            $address .= ucwords(strtolower($data['StandardFields']['StreetNumber'])) .' ';
        }
        if (!empty($data['StandardFields']['StreetName']) &&
            $data['StandardFields']['StreetName']    != '********') {
            $address .= ucwords(strtolower($data['StandardFields']['StreetName']))   .' ';
        }
        if (!empty($data['StandardFields']['StreetSuffix']) &&
            $data['StandardFields']['StreetSuffix']  != '********') {
            $address .= ucwords(strtolower($data['StandardFields']['StreetSuffix']))  .' ';
        }

        $search = array(
            'Street', 'Drive', 'Road', 'Avenue', 'Court', 'Lane', 'Boulevard'
        );
        $replace = array(
            'St', 'Dr', 'Rd', 'Ave', 'Ct', 'Ln', 'Blvd'
        );

        return str_replace($search, $replace, $address);
    }
}