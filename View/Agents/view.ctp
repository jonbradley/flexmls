<div class="container">
  <div class="row">
    <?php echo $this->element('sidebar', array('url' => '/search')); ?>
    <div id="main" class="span9">

      <h1 class="page-header"><?php echo $data['Agent']['name']; ?></h1>

      <div class="agent">
        <div class="row">
          <div class="image span2">
            <?php echo $this->Html->image('agents/'. $data['Agent']['photo'], array('alt' => $data['Agent']['name'])); ?>

            <br /><br />

            <?php if (!empty($data['Agent']['phone'])) { ?>
              <div class="phone">
                <i class="icon icon-normal-mobile-phone"></i>
                <?php echo $this->Html->link($data['Agent']['phone'], 'tel://'. $data['Agent']['phone']); ?>
              </div>
            <?php } ?>

            <?php if (!empty($data['Agent']['fax'])) { ?>
              <div class="phone">
                <i class="icon icon-normal-phone"></i>
                <?php echo $this->Html->link($data['Agent']['fax'], 'tel://'. $data['Agent']['fax']); ?>
              </div>
            <?php } ?>

            <?php if (!empty($data['Agent']['email'])) { ?>
              <div class="email">
                <i class="icon icon-normal-mail"></i>
                <?php echo $this->Html->link('Click Here', 'mailto:'. $data['Agent']['email']); ?>
              </div>
            <?php } ?>
          </div><!-- /.image -->

          <div class="body span7">
            <p><?php echo $data['Agent']['bio']; ?></p>
          </div>
        </div>
      </div>

      <hr />

      <h2>Our Recent Properties</h2>
      <div class="properties-grid">
        <div class="row-fluid">
          <?php 
            $i = 1;
            foreach ($listings as $key => $value) {
              echo $this->element('../Listings/_recent_properties', array('data' => $value, 'i' => $i));
              $i++; 
            }
          ?>
          </div>
        </div>
      </div>
      <div class="show-all">
        <a href="/search">Show all</a>
      </div>   

    </div>

  </div>
</div>