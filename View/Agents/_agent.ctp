<div class="agent span3">
  <div class="image">
    <?php 
    echo $this->Html->link(
      $this->Html->image('agents/'. $data['Agent']['photo'], array('alt' => $data['Agent']['name'], 'style' => 'height: 267px; width: 201px;')),
      '/agents/'. $data['Agent']['slug'] .'.html',
      array('escape' => false)
      ); 
    ?>
  </div><!-- /.image -->

  <div class="body">
  <h3>
    <?php 
      echo $this->Html->link(
        $data['Agent']['name'],
        '/agents/'. $data['Agent']['slug'] .'.html',
        array('escape' => false)
      ); 
    ?>
  </h3>

  </div><!-- /.body -->

  <div class="info">
    <div class="box">

      <?php if (!empty($data['Agent']['phone'])) { ?>
        <div class="phone">
          <i class="icon icon-normal-mobile-phone"></i>
          <?php echo $this->Html->link($data['Agent']['phone'], 'tel://'. $data['Agent']['phone']); ?>
        </div><!-- /.phone -->
      <?php } ?>

      <?php if (!empty($data['Agent']['fax'])) { ?>
        <div class="office">
          <i class="icon icon-normal-phone"></i>
          <?php echo $this->Html->link($data['Agent']['fax'], 'tel://'. $data['Agent']['fax']); ?>
        </div>
      <?php } ?>

      <?php if (!empty($data['Agent']['email'])) { ?>
        <div class="email">
          <i class="icon icon-normal-mail"></i>
          <?php echo $this->Html->link('Click Here', 'mailto:'. $data['Agent']['email']); ?>
        </div>
      <?php } ?>

    </div>

  </div>
</div>

<?php
if (($i%4) == 0) 
  echo '</div><div class="row-fluid">';
?>
