<div class="container">
  <div class="row">
    <?php echo $this->element('sidebar', array('url' => '/search')); ?>
    <div id="main" class="span9 agents-list">

    <h1 class="page-header">Agents</h1>

    <div class="our-agents-large">
      <div class="row-fluid">
        <?php 
          $i=1;
          foreach ($data as $key => $value) { 
            echo $this->element('../Agents/_agent', array('data' => $value, 'i' => $i));
            $i++;
          }
        ?>
      </div>
    </div>
  </div>
</div>