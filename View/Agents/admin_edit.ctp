<h2>Edit Agent</h2>
<div class="users form">
<?php echo $this->Form->create('Agent', array('type' => 'file')); ?>
    <fieldset>
        <?php 
        echo $this->Form->input('firstname', array('style' => 'width: 100%;'));
        echo $this->Form->input('lastname', array('style' => 'width: 100%;'));
        echo $this->Form->input('name', array('style' => 'width: 100%;', 'label' => 'Display Name'));
        echo $this->Form->input('bio', array('style' => 'width: 100%; height: 200px'));
        echo $this->Form->input('address', array('style' => 'width: 100%; height: 100px'));
        echo $this->Form->input('phone', array('style' => 'width: 100%;'));
        echo $this->Form->input('fax', array('style' => 'width: 100%;'));
        echo $this->Form->input('email', array('style' => 'width: 100%;'));
        echo $this->Form->input('image', array('style' => 'width: 100%;', 'type' => 'file'));
        echo $this->Form->input('slug', array('style' => 'width: 100%;'));
    ?>
    </fieldset>
<?php 
echo $this->Form->submit('Submit', array('class' => 'btn')); 
echo $this->Form->end();
?>
</div>