<div id="contact">
<?php 
echo $this->Session->flash(); 
echo $this->Form->create('Contact', array('url' => '/contacts/submit', 'class' => 'form-horizontal')); 
echo $this->Form->hidden('title', array('value' => 'Contact Us'));
echo $this->Form->hidden('ip_address', array('value' => $_SERVER['REMOTE_ADDR']));
?>

<br />

<div class="row-fluid">
  <div class="span6">
    <?php 
      echo $this->Form->input('name', array(
        'label' => 'Your Name',
        'type' => 'text',
      ));
    ?>
  </div>
  <div class="span6">
    <?php 
      echo $this->Form->input('phone_number', array(
        'label' => 'Phone Number',
        'type' => 'text',
      ));
    ?>
  </div>
</div>

<br />

<div class="row-fluid">
  <div class="span6">
    <?php 
      echo $this->Form->input('email', array(
        'label' => 'Email Address',
        'type' => 'text',
      ));
    ?>
  </div>
  <div class="span6">
    <?php 
      echo $this->Form->input('best_time_to_contact', array(
        'label' => 'Best Time to Contact',
        'type' => 'text',
      ));
    ?>
  </div>
</div>

<br />

<div class="row-fluid">
  <div class="span6">
    <?php 
      echo $this->Form->input('reason_for_contacting', array(
        'label' => 'Reason For Contacting',
        'type' => 'select',
        'options' => array(
          'Selling' => 'Selling',
          'Buying' => 'Buying',
          'Leasing' => 'Leasing',
          'Other' => 'Other'
        )
      ));
    ?>
  </div>
  <div class="span6">
    <?php 
      echo $this->Form->input('how_did_you_find_us', array(
        'label' => 'How Did You Find Us',
        'type' => 'select',
        'options' => array(
          'Google/Yahoo/MSN' => 'Google/Yahoo/MSN',
          'Previous Client' => 'Previous Client',
          'Referral from Friend' => 'Referral from Friend',
          'Advertisement' => 'Advertisement',
          'Other' => 'Other'  
        )
      ));
    ?>
  </div>
</div>

<br />

<div class="row-fluid">
  <div class="span6">
    <?php 
      echo $this->Form->input('comments', array(
        'label' => 'Comments',
        'div' => false,
        'required' => false,
        'type' => 'textarea',
      ));
    ?>
  </div>
  <div class="span6">
    <?php 
      echo $this->Form->submit('Send', array('class' => 'btn btn-info btn btn-large'));
    ?>
  </div>
</div>

<?php

echo $this->Form->end();
?>
</div>