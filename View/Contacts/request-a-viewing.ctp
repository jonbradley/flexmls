<div id="contact">
<?php 
echo $this->Session->flash(); 
echo $this->Form->create('Contact', array('url' => '/contacts/submit')); 
echo $this->Form->hidden('title', array('value' => 'Request a Viewing'));
echo $this->Form->hidden('ip_address', array('value' => $_SERVER['REMOTE_ADDR']));
?>

<br />

<div class="row-fluid">
  <div class="span6">
    <?php 
      echo $this->Form->input('name', array(
        'label' => 'Your Name',
        'type' => 'text',
      ));
      echo $this->Form->input('phone_number', array(
        'label' => 'Phone Number',
        'type' => 'text',
      ));
      echo $this->Form->input('email', array(
        'label' => 'Email Address',
        'type' => 'text',
      ));
      echo $this->Form->input('best_time_to_contact', array(
        'label' => 'Best Time to Contact',
        'type' => 'text',
      ));
    ?>
  </div>
</div>

<br />

<div class="row-fluid">
  <div class="span6">
  </div>
  <div class="span6">
    <?php 
      echo $this->Form->submit('Submit Viewing Request', array('class' => 'btn btn-info btn'));
    ?>
  </div>
</div>

<?php

echo $this->Form->end();
?>
</div>