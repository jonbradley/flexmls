<?php 
echo $this->Form->create('newsletter', array('controller' => 'Contacts', 'view' => 'submit', 'class' => 'form-horizontal')); 
echo $this->Form->hidden('title', array('value' => 'Newsletter Sign Up'));
?>

<div class="row">
    <?php 
      echo $this->Form->input('email', array(
        'label' => false,
        'div' => 'required',
        'required' => true,
        'type' => 'text',
        'placeholder' => 'Enter Your Email'
      ));
    ?>
</div>

<br />

<div class="row">
    <?php 
      echo $this->Form->submit('Subscribe', array('class' => 'btn btn-info btn btn-large'));
    ?>
</div>

<?php

echo $this->Form->end();
?>